<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api/v1/nodeb', 'middleware'=>'auth'], function() use($router){
  $router->get('/', 'NodebController@index');
  $router->get('unknown', 'NodebController@unknown');
  // $router->post('create', 'NodebController@create');
  $router->post('insert', 'NodebController@insert');
  $router->post('update', ['uses'=>'NodebController@update']);
  $router->post('update_unknown', 'NodebController@update_unknown');
  // $router->delete('/{id}', 'NodebController@destroy');
});

// $router->get('api/v1/test', 'NodebController@index2');
$router->post('api/v1/update_ticket', 'TicketingController@update_ticket');
$router->post('api/v1/update_ticket_unknown', 'TicketingController@update_ticket_unknown');
$router->post('api/v1/update_ticket_mom', 'TicketingController@update_ticket_mom');
$router->post('api/v1/update_ticket_nodin', 'TicketingController@update_ticket_nodin');

$router->post('api/v1/upload', 'NodebController@upload');
$router->get('api/v1/summary', 'NodebController@summary');
$router->get('api/v1/get_status', 'NodebController@get_status');

$router->group(['prefix' => 'api/user'], function() use($router){
    $router->post('insert', 'UserController@insert_user');
    $router->post('login', 'UserController@login');
    $router->post('update', 'UserController@update_user');
});

$router->group(['prefix' => 'api/integrasi'], function() use($router){
  $router->get('test_traffic','IntegrasiAgataController@test_traffic');
  $router->get('test_connection','IntegrasiAgataController@test_connection');
  $router->get('token_bigdata', 'IntegrasiAgataController@get_token_bigdata');

  //FTP
  $router->get('ftp_bigdata_erricson','IntegrasiAgataController@get_ftp_file_erricson');
  $router->get('ftp_bigdata_zte','IntegrasiAgataController@get_ftp_file_zte');
  $router->get('ftp_bigdata_huawei','IntegrasiAgataController@get_ftp_file_huawei');
  $router->get('ftp_bigdata_nokia','IntegrasiAgataController@get_ftp_file_nokia');

  //ZTE
  $router->get('integrasi_agata_zte_2g', 'IntegrasiAgataController@integrasi_agata_zte_2g');
  $router->get('integrasi_agata_zte_3g', 'IntegrasiAgataController@integrasi_agata_zte_3g');
  $router->get('integrasi_agata_zte_4g', 'IntegrasiAgataController@integrasi_agata_zte_4g');

  // ERRICSON
  $router->get('integrasi_agata_erricson_2g', 'IntegrasiAgataController@integrasi_agata_erricson_2g');
  $router->get('integrasi_agata_erricson_3g', 'IntegrasiAgataController@integrasi_agata_erricson_3g');
  $router->get('integrasi_agata_erricson_4g', 'IntegrasiAgataController@integrasi_agata_erricson_4g');
});

$router->get('api/bts_summary_1', 'DocumentController@bts_summary_1');
$router->get('api/bts_summary_2', 'DocumentController@bts_summary_2');
$router->get('api/bts_summary_2_monthly', 'DocumentController@bts_summary_2_monthly');
$router->get('api/bts_summary_2_monthly1', 'DocumentController@bts_summary_2_monthly1');
$router->get('api/bts_summary_3', 'DocumentController@bts_summary_3');
$router->get('api/bts_summary_4', 'DocumentController@bts_summary_4');
$router->get('api/bts_summary_dismantle', 'DocumentController@bts_summary_dismantle');
$router->get('api/bts_summary_check', 'DocumentController@bts_summary_check');
$router->get('api/bts_summary_needcheck', 'DocumentController@bts_summary_needcheck');
$router->get('api/bts_summary_needcheck_2g', 'DocumentController@bts_summary_needcheck_2g');
$router->get('api/bts_summary_needcheck_3g', 'DocumentController@bts_summary_needcheck_3g');
$router->get('api/bts_summary_needcheck_4g', 'DocumentController@bts_summary_needcheck_4g');
$router->get('api/table_list_mom', 'DocumentController@table_list_mom');
$router->get('api/table_list_nodin', 'DocumentController@table_list_nodin');
$router->get('api/resume_ne', 'DocumentController@resume_ne');
$router->get('api/resume_ne_monthly', 'DocumentController@resume_ne_monthly');
$router->get('api/resume_site', 'DocumentController@resume_site');
$router->get('api/resume_site_monthly', 'DocumentController@resume_site_monthly');
$router->get('api/resume_nodin_bts_history', 'DocumentController@resume_nodin_bts_history');
$router->get('api/chart_onair', 'DocumentController@chart_onair');
$router->get('api/chart_onair_monthly', 'DocumentController@chart_onair_monthly');

$router->get('api/dashboard_2g', 'DocumentController@dashboard_2g');
$router->get('api/dashboard_3g', 'DocumentController@dashboard_3g'); 
$router->get('api/dashboard_4g', 'DocumentController@dashboard_4g');
$router->get('api/dashboard_total', 'DocumentController@dashboard_total');
$router->get('api/dashboard_2g_monthly', 'DocumentController@dashboard_2g_monthly');
$router->get('api/dashboard_3g_monthly', 'DocumentController@dashboard_3g_monthly');
$router->get('api/dashboard_4g_monthly', 'DocumentController@dashboard_4g_monthly');
$router->get('api/dashboard_total_monthly', 'DocumentController@dashboard_total_monthly');

$router->get('master_baseline_onair_4g_update_jmlh', 'CrontabController@master_baseline_onair_4g_update_jmlh');
$router->get('master_baseline_onair_4g_monthly_update_jmlh', 'CrontabController@master_baseline_onair_4g_monthly_update_jmlh');

$router->get('api/onair_bts_4g', 'BaselineController@onair_bts_4g');
$router->get('api/onair_bts_3g', 'BaselineController@onair_bts_3g');
$router->get('api/onair_bts_2g', 'BaselineController@onair_bts_2g'); 
$router->put('api/onair_bts_4g_update', 'BaselineController@onair_bts_4g_update');
$router->put('api/onair_bts_3g_update', 'BaselineController@onair_bts_3g_update');
$router->put('api/onair_bts_2g_update', 'BaselineController@onair_bts_2g_update');

$router->post('api/sysinfo4g', 'BaselineController@sysinfo4g');
$router->post('api/sysinfo3g', 'BaselineController@sysinfo3g');
$router->post('api/sysinfo2g', 'BaselineController@sysinfo2g');
$router->get('api/deleteData/{id}', 'BaselineController@deleteData');

$router->get('cron/resume_nodin_bts_checking', 'CrontabController@resume_nodin_bts_checking');
$router->get('cron/resume_nodin_bts_monthly', 'CrontabController@resume_nodin_bts_monthly');
$router->get('cron/resume_nodin_bts', 'CrontabController@resume_nodin_bts');
$router->get('cron/resume_ne', 'CrontabController@resume_ne');
$router->get('cron/resume_ne_monthly', 'CrontabController@resume_ne_monthly');
$router->get('cron/resume_ne_per_band_monthly', 'CrontabController@resume_ne_per_band_monthly');

$router->get('api/onair_bts_4g_all', 'BaselineController@onair_bts_4g_all');
$router->get('api/onair_bts_3g_all', 'BaselineController@onair_bts_3g_all');
$router->get('api/onair_bts_2g_all', 'BaselineController@onair_bts_2g_all');

// $router->get('api/baseline_on_air_4g_monthly', 'NodebController@baseline_on_air_4g_monthly');
$router->get('api/baseline_on_air_4g_monthly', 'BaselineController@onair_bts_4g');

// $router->post('api/insert_nodin', 'DocumentController@insert_nodin');
// $router->post('api/approve_nodin', 'DocumentController@approve_nodin');
