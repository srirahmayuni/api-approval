<?php
namespace App;

use DB;
use Log;

class Ticketing
{
    const URL = 'http://10.54.36.49/apis-workflow/index.php/';

    static function post_ticket($data)
    {
        $url = self::URL.'ManagerController';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('POST', $url, [
                'json' => [
                    'id_ticket' => $data['id_ticket'],
                    'submitted_by' => $data['submitted_by'],
                    'manager' => $data['manager'],
                    'data' => $data['data']
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        return json_decode($res->getBody(), true);
    }

}
?>
