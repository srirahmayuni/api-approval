<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use DB;
use Carbon\Carbon;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    // protected $auth;
    //
    // /**
    //  * Create a new middleware instance.
    //  *
    //  * @param  \Illuminate\Contracts\Auth\Factory  $auth
    //  * @return void
    //  */
    // public function __construct(Auth $auth)
    // {
    //     $this->auth = $auth;
    // }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if ($this->auth->guard($guard)->guest()) {
        //     return response('Unauthorized.', 401);
        // }
        //
        // return $next($request);

        $check = DB::table('t_token')
        ->where('token', $request->input('token'))
        ->where('end_date', '>', Carbon::now()->toDateTimeString())
        ->first();
        if ($check) {
            return $next($request);
        }else {
            return response()->json('Unauthorized', 401);
        }

    }
}
