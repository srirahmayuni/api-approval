<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Log;
use Carbon\Carbon;

/**
 *
 */
class CrontabController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function resume_ne()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            try {
                ini_set('max_execution_time', 86400);
                DB::connection('mysql2')
                ->table('resume_ne')
                ->whereDate('input_date', date('Y-m-d'))
                ->delete();

                $data_before = DB::connection('mysql2')
                ->select('select
                regional,
                vendor,
                2g_bsc as bsc_count,
                2g_site as site_2g_count,
                2g_bts as bts_2g_count,
                2g_sector as sector_2g_count,
                2g_trx as trx_count,
                3g_rnc as rnc_count,
                3g_f1_f2 as f1f2_count,
                3g_6sector as sec6_count,
                3g_f3 as f3_count,
                3g_bts_hotel as bts_hotel_count,
                3g_sector as sector_3g_count,
                4g_enodeb as enodeb_count,
                4g_sector as sector_4g_count,
                input_date
                from resume_ne where DATE(input_date) = DATE(NOW()) - interval 1 day
                order by substr(regional, 9, 2)*1 asc, vendor asc');

                $all_data = $data_before;

                for ($i=0; $i < count($all_data); $i++) {
                    $data_4g = DB::connection('mysql2')
                    ->select('select
                    REGIONAL,
                    VENDOR,
                    count(distinct(NEID)) as neid_count,
                    count(distinct(CELL_NAME)) as cellname_count
                    from master_baseline_onair_4g
                    where DATE(DATE_ONAIR) = DATE(NOW()) - interval 1 day
                    and REGIONAL = "'.$all_data[$i]->regional.'"
                    and VENDOR = "'.$all_data[$i]->vendor.'"');
                    $all_data[$i]->enodeb_count = $data_4g[0]->neid_count + $data_before[$i]->enodeb_count;
                    $all_data[$i]->cellname_count = $data_4g[0]->cellname_count + $data_before[$i]->sector_4g_count;
                }

                $i=0;
                foreach ($all_data as $data) {
                    DB::connection('mysql2')
                    ->table('resume_ne')
                    ->insert([
                        'regional' => $data->regional,
                        'vendor' => $data->vendor,
                        '2g_bsc' => $data_before[$i]->bsc_count,
                        '2g_site' => $data_before[$i]->site_2g_count,
                        '2g_bts' => $data_before[$i]->bts_2g_count,
                        '2g_sector' => $data_before[$i]->sector_2g_count,
                        '2g_trx' => $data_before[$i]->trx_count,
                        '3g_rnc' => $data_before[$i]->rnc_count,
                        '3g_f1_f2' => $data_before[$i]->f1f2_count,
                        '3g_6sector' => $data_before[$i]->sec6_count,
                        '3g_f3' => $data_before[$i]->f3_count,
                        '3g_bts_hotel' => $data_before[$i]->bts_hotel_count,
                        '3g_sector' => $data_before[$i]->sector_3g_count,
                        '4g_enodeb' => $all_data[$i]->enodeb_count,
                        '4g_sector' => $all_data[$i]->cellname_count,
                        'input_date' => date('Y-m-d H:i:s')
                    ]);
                    $i++;
                }

                Log::info('Done resume_ne');
                echo "Done";
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                echo $e->getMessage();
            }

        }else {
            return response('Unauthorized', 400);
        }
    }

    public function resume_nodin_bts()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            try {
                DB::connection('mysql2')
                ->table('resume_nodin_bts')
                ->whereDate('date', date('Y-m-d'))
                ->delete();

                $last_record = DB::connection('mysql2')
                ->table('resume_nodin_bts')
                ->orderBy('date', 'desc')
                ->first();

                $onair_4g = DB::connection('mysql2')
                ->select('select
                count(*) as count_enodeb
                from auto_onair_dafinci WHERE
                DATE(On_Air_Date) = DATE(NOW()) - interval 1 day');

                // $trx = DB::select('select sum(TRX) as sum_trx from auto_stylo_master_cell_2g');

                $resume_site = DB::connection('mysql2')
                ->select('select
                SUM(2g_only) as only_2g,
                SUM(3g_only) as only_3g,
                SUM(4g_only) as only_4g,
                SUM(2g3g) + SUM(2g4g) + SUM(3g4g) + SUM(2g3g4g) as collocation
                from resume_site where DATE(input_date) = DATE(NOW())');

                DB::connection('mysql2')
                ->table('resume_nodin_bts')
                ->insert([
                    'date' => date('Y-m-d'),
                    'onair_2g' => 0,
                    'offair_2g' => 0,
                    'total_2g' => $last_record->total_2g,
                    'onair_3g' => 0,
                    'offair_3g' => 0,
                    'total_3g' => $last_record->total_3g,
                    'onair_4g' => $onair_4g[0]->count_enodeb,
                    'offair_4g' => 0,
                    'total_4g' => $last_record->total_4g + $onair_4g[0]->count_enodeb,
                    'onair_trx' => 0,
                    'offair_trx' => 0,
                    'total_trx' => $last_record->total_trx,
                    'total_all' => $last_record->total_2g + $last_record->total_3g + $last_record->total_4g + $onair_4g[0]->count_enodeb,
                    'only_2g' => $resume_site[0]->only_2g,
                    'only_3g' => $resume_site[0]->only_3g,
                    'only_4g' => $resume_site[0]->only_4g,
                    'collocation' => $resume_site[0]->collocation,
                    'total_all_site' => $resume_site[0]->only_2g + $resume_site[0]->only_3g + $resume_site[0]->only_4g + $resume_site[0]->collocation,
                    'input_date' => date('Y-m-d H:i:s')
                ]);

                return "Done";
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                Log::error($e->getMessage());
            }
        }else {
            return response('Unauthorized', 400);
        }
    }

    public function master_baseline_onair_4g_update_jmlh()
    {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $hapus = DB::connection('mysql2')
        ->table('master_baseline_onair_4g')
        ->update([
            'JMLH_ENODEB' => ''
        ]);

        echo "Hapus";

        $satu = DB::connection('mysql2')
        ->table('master_baseline_onair_4g')
        ->whereRaw('substr(CI, 1, 1) = 1 and length(CI) = 2')
        ->update([
            'JMLH_ENODEB' => 1
        ]);

        $null = DB::connection('mysql2')
        ->table('master_baseline_onair_4g')
        ->where('CI', null)
        ->update([
            'JMLH_ENODEB' => 1
        ]);

        echo "Satu";

        $dua = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 2)');

        echo "Dua";

        $tiga = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 3)');

        echo "Tiga";

        $empat = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 4)');

        echo "Empat";

        $lima = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 5)');

        echo "Lima";

        $enam = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 6)');

        echo "Enam";

        $tujuh = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 7 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 7)');

        echo "Tujuh";

        $delapan = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 7 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 8 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 8)');

        echo "Delapan";

        $sembilan = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 7 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 8 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 9 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 9)');

        echo "Sembilan";

        $nol = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 7 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 8 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 9 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 0 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 0)');

        echo "Nol";

        $jmlh_cell = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g set JMLH_CELL = 1');

        return "Done";

    }

    public function master_baseline_onair_4g_monthly_update_jmlh()
    {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $month = date('m');

        $hapus = DB::connection('mysql2')
        ->table('master_baseline_onair_4g_monthly')
        ->whereMonth('INPUT_DATE', $month)
        ->update([
            'JMLH_ENODEB' => ''
        ]);

        echo "Hapus";

        $satu = DB::connection('mysql2')
        ->table('master_baseline_onair_4g_monthly')
        ->whereRaw('substr(CI, 1, 1) = 1 and length(CI) = 2')
        ->whereMonth('INPUT_DATE', $month)
        ->update([
            'JMLH_ENODEB' => 1
        ]);

        $null = DB::connection('mysql2')
        ->table('master_baseline_onair_4g_monthly')
        ->where('CI', null)
        ->whereMonth('INPUT_DATE', $month)
        ->update([
            'JMLH_ENODEB' => 1
        ]);

        echo "Satu";

        $dua = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 2 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Dua";

        $tiga = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 3 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Tiga";

        $empat = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 4 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Empat";

        $lima = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 5 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Lima";

        $enam = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 6 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Enam";

        $tujuh = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 7 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 7 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Tujuh";

        $delapan = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 7 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 8 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 8 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Delapan";

        $sembilan = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 7 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 8 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 9 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 9 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Sembilan";

        $nol = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_ENODEB = 1 where CELL_NAME in (
            select CELL_NAME from (select * from master_baseline_onair_4g_monthly group by NEID having
            max(case when substr(CI, 1, 1) = 1 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 2 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 3 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 4 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 5 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 6 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 7 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 8 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 9 then 1 else 0 end) = 0 and
            max(case when substr(CI, 1, 1) = 0 then 1 else 0 end) = 1) a where substr(CI, 1, 1) = 0 and MONTH(INPUT_DATE) = '.$month.')');

        echo "Nol";

        $jmlh_cell = DB::connection('mysql2')
        ->update('update master_baseline_onair_4g_monthly set JMLH_CELL = 1');

        return "Done";

    }

    public function resume_nodin_bts_checking()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            try {
                for ($i=date('d', strtotime('-1 days')); $i >= 0; $i--) {
                    $k = $i+1;
                    $date = date('Y-m-d', strtotime('-'.$i.' days'));
                    $date_before = date('Y-m-d', strtotime('-'.$k.' days'));
                    $datetime = date('Y-m-d 00:00:00', strtotime('-'.$i.' days'));

                    $last_record = DB::connection('mysql2')
                    ->table('resume_nodin_bts')
                    ->orderBy('date', 'desc')
                    ->where('date', $date_before)
                    ->first();

                    $now_record = DB::connection('mysql2')
                    ->table('resume_nodin_bts')
                    ->where('date', $date)
                    ->first();

                    $onair_4g = DB::connection('mysql2')
                    ->select('select
                    count(*) as count_enodeb
                    from auto_onair_dafinci WHERE
                    DATE(On_Air_Date) = "'.$date.'"');

                    DB::connection('mysql2')
                    ->table('resume_nodin_bts')
                    ->where('date', $date)
                    ->update([
                        'onair_4g' => $onair_4g[0]->count_enodeb,
                        'offair_4g' => $now_record->offair_4g,
                        'total_4g' => $last_record->total_4g + $onair_4g[0]->count_enodeb - $now_record->offair_4g,
                        'total_all' => $now_record->total_2g + $now_record->total_3g + $last_record->total_4g + $onair_4g[0]->count_enodeb - $now_record->offair_4g
                    ]);
                    var_dump($date.'<br>');
                }
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                Log::error($e->getMessage());
            }
        }else {
            return response('Unauthorized', 400);
        }
    }

    public function resume_nodin_bts_monthly()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            try {
                $date = date('Y-m-t', strtotime('-1 month'));

                $last_record = DB::connection('mysql2')
                ->table('resume_nodin_bts_monthly')
                ->orderBy('date', 'desc')
                ->first();

                $resume_nodin_bts = DB::connection('mysql2')
                ->table('resume_nodin_bts')
                ->where('date', $date)
                ->first();

                $onair_4g = DB::connection('mysql2')
                ->select('select
                count(*) as count_enodeb
                from auto_onair_dafinci_monthly');

                DB::connection('mysql2')
                ->table('resume_nodin_bts_monthly')
                ->insert([
                    'date' => $date,
                    'onair_2g' => 0,
                    'offair_2g' => 0,
                    'total_2g' => $last_record->total_2g,
                    'onair_3g' => 0,
                    'offair_3g' => 0,
                    'total_3g' => $last_record->total_3g,
                    'onair_4g' => $onair_4g[0]->count_enodeb,
                    'offair_4g' => 0,
                    'total_4g' => $last_record->total_4g + $onair_4g[0]->count_enodeb,
                    'onair_trx' => 0,
                    'offair_trx' => 0,
                    'total_trx' => $last_record->total_trx,
                    'total_all' => $last_record->total_2g + $last_record->total_3g + $last_record->total_4g + $onair_4g[0]->count_enodeb,
                    'only_2g' => $resume_nodin_bts->only_2g,
                    'only_3g' => $resume_nodin_bts->only_3g,
                    'only_4g' => $resume_nodin_bts->only_4g,
                    'collocation' => $resume_nodin_bts->collocation,
                    'total_all_site' => $resume_nodin_bts->total_all_site
                ]);

                return "Done";
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                Log::error($e->getMessage());
            }

        }else {
            return response('Unauthorized', 400);
        }
    }

    public function resume_ne_monthly()
    {
        if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '::1') {
            try {
                ini_set('max_execution_time', 86400);
                $date = date('Y-m-t', strtotime('-1 month'));
                $date_before = date('Y-m-t', strtotime('-2 month'));
                $datetime = date('Y-m-t 00:00:00', strtotime('-1 month'));

                DB::connection('mysql2')
                ->table('resume_ne_monthly')
                ->whereDate('input_date', $date)
                ->delete();

                $data_before = DB::connection('mysql2')
                ->select('select
                regional,
                vendor,
                2g_bsc as bsc_count,
                2g_site as site_2g_count,
                2g_bts as bts_2g_count,
                2g_sector as sector_2g_count,
                2g_trx as trx_count,
                3g_rnc as rnc_count,
                3g_f1_f2 as f1f2_count,
                3g_6sector as sec6_count,
                3g_f3 as f3_count,
                3g_bts_hotel as bts_hotel_count,
                3g_sector as sector_3g_count,
                4g_enodeb as enodeb_count,
                4g_sector as sector_4g_count,
                input_date
                from resume_ne_monthly where DATE(input_date) = "'.$date_before.'"
                order by substr(regional, 9, 2)*1 asc, vendor asc');

                $all_data = $data_before;

                for ($i=0; $i < count($all_data); $i++) {
                    $data_4g = DB::connection('mysql2')
                    ->select('select
                    REGIONAL,
                    VENDOR,
                    count(distinct(NEID)) as neid_count,
                    count(distinct(CELL_NAME)) as cellname_count
                    from master_baseline_onair_4g_monthly
                    where MONTH(DATE_ONAIR) = MONTH(NOW()-interval 1 month)
                    and REGIONAL = "'.$all_data[$i]->regional.'"
                    and VENDOR = "'.$all_data[$i]->vendor.'"');
                    $all_data[$i]->enodeb_count = $data_4g[0]->neid_count + $data_before[$i]->enodeb_count;
                    $all_data[$i]->cellname_count = $data_4g[0]->cellname_count + $data_before[$i]->sector_4g_count;
                }

                $i=0;
                foreach ($all_data as $data) {
                    DB::connection('mysql2')
                    ->table('resume_ne_monthly')
                    ->insert([
                        'regional' => $data->regional,
                        'vendor' => $data->vendor,
                        '2g_bsc' => $data_before[$i]->bsc_count,
                        '2g_site' => $data_before[$i]->site_2g_count,
                        '2g_bts' => $data_before[$i]->bts_2g_count,
                        '2g_sector' => $data_before[$i]->sector_2g_count,
                        '2g_trx' => $data_before[$i]->trx_count,
                        '3g_rnc' => $data_before[$i]->rnc_count,
                        '3g_f1_f2' => $data_before[$i]->f1f2_count,
                        '3g_6sector' => $data_before[$i]->sec6_count,
                        '3g_f3' => $data_before[$i]->f3_count,
                        '3g_bts_hotel' => $data_before[$i]->bts_hotel_count,
                        '3g_sector' => $data_before[$i]->sector_3g_count,
                        '4g_enodeb' => $all_data[$i]->enodeb_count,
                        '4g_sector' => $all_data[$i]->cellname_count,
                        'input_date' => $datetime
                    ]);
                    $i++;
                }

                return "Done";
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                Log::error($e->getMessage());
            }

        }else {
            return response('Unauthorized', 400);
        }
    }

    public function resume_ne_per_provinsi_monthly()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            try {
                ini_set('max_execution_time', 86400);

                $date = date('Y-m-t', strtotime('-1 month'));

                DB::connection('mysql2')
                ->table('resume_ne')
                ->whereDate('input_date', $date)
                ->delete();

                Log::info('Done resume_ne');
                echo "Done";
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                echo $e->getMessage();
            }

        }else {
            return response('Unauthorized', 400);
        }
    }

    public function resume_ne_per_band_monthly()
    {
        $data_before = DB::connection('mysql2')
        ->table('resume_ne_per_band_monthly')
        ->select('regional', 'vendor', '2g_bsc as bsc', '2g_site as site', '2g_dcs as dcs', '2g_gsm as gsm', '2g_sector as sector_2g',
        '2g_trx as trx', '3g_rnc as rnc', '3g_f1_f2 as f1f2', '3g_6sector as sec6', '3g_f3 as f3', '3g_bts_hotel as bts_hotel',
        '3g_u900 as u900', '3g_sector as sector_3g', '4g_lte900 as lte900', '4g_lte1800 as lte1800', '4g_lte2100 as lte2100',
        '4g_lte2300 as lte2300', '4g_sector as sector_4g')
        ->where('input_date', null)
        ->get();

        foreach ($data_before as $data_bef) {
            $data_dcs = DB::connection('mysql2')
            ->select('select SUM(JMLH_BTS) as dcs from master_baseline_onair_2g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE" and TYPE_FREQ = "DCS"');
            $data_gsm = DB::connection('mysql2')
            ->select('select SUM(JMLH_BTS) as gsm from master_baseline_onair_2g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE" and TYPE_FREQ = "GSM"');
            $data_bsc = DB::connection('mysql2')
            ->select('select SUM(JMLH_BSC) as bsc from master_baseline_onair_2g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE"');
            $data_site = DB::connection('mysql2')
            ->select('select SUM(JMLH_SITE) as site from master_baseline_onair_2g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE"');
            $data_sector_2g = DB::connection('mysql2')
            ->select('select SUM(JMLH_CELL) as sector_2g from master_baseline_onair_2g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE"');

            $data_lte900 = DB::connection('mysql2')
            ->select('select SUM(JMLH_ENODEB) as lte900 from master_baseline_onair_4g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE" and BANDTYPE like "%LTE900%" ');
            $data_lte1800 = DB::connection('mysql2')
            ->select('select SUM(JMLH_ENODEB) as lte1800 from master_baseline_onair_4g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE" and BANDTYPE like "%LTE1800%" ');
            $data_lte2100 = DB::connection('mysql2')
            ->select('select SUM(JMLH_ENODEB) as lte2100 from master_baseline_onair_4g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE" and BANDTYPE like "%LTE2100%" ');
            $data_lte2300 = DB::connection('mysql2')
            ->select('select SUM(JMLH_ENODEB) as lte2300 from master_baseline_onair_4g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE" and BANDTYPE like "%LTE2300%" ');
            $data_sector_4g = DB::connection('mysql2')
            ->select('select SUM(JMLH_CELL) as sector_4g from master_baseline_onair_4g_monthly
            where REGIONAL = "'.$data_bef->regional.'" and VENDOR = "'.$data_bef->vendor.'"
            and STATUS_BTS != "DISMANTLE"');

            DB::connection('mysql2')
            ->table('resume_ne_per_band_monthly')
            ->insert([
                'regional' => $data_bef->regional,
                'vendor' => $data_bef->vendor,
                '2g_bsc' => $data_bsc[0]->bsc,
                '2g_site' => $data_site[0]->site,
                '2g_dcs' => $data_dcs[0]->dcs,
                '2g_gsm' => $data_gsm[0]->gsm,
                '2g_sector' => $data_sector_2g[0]->sector_2g,
                '2g_trx' => $data_bef->trx,
                '3g_rnc' => $data_bef->rnc,
                '3g_f1_f2' => $data_bef->f1f2,
                '3g_6sector' => $data_bef->sec6,
                '3g_f3' => $data_bef->f3,
                '3g_bts_hotel' => $data_bef->bts_hotel,
                '3g_u900' => $data_bef->u900,
                '3g_sector' => $data_bef->sector_3g,
                '4g_lte900' => $data_lte900[0]->lte900,
                '4g_lte1800' => $data_lte1800[0]->lte1800,
                '4g_lte2100' => $data_lte2100[0]->lte2100,
                '4g_lte2300' => $data_lte2300[0]->lte2300,
                '4g_sector' => $data_sector_4g[0]->sector_4g,
                'input_date' => '2019-03-30 00:00:00'
            ]);
        }
    }

    public function resume_ne_checking()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            try {
                for ($i=date('d', strtotime('-1 days')); $i >= 0; $i--) {
                    $k = $i+1;
                    $date = date('Y-m-d', strtotime('-'.$i.' days'));
                    $date_before = date('Y-m-d', strtotime('-'.$k.' days'));
                    $datetime = date('Y-m-d 00:00:00', strtotime('-'.$i.' days'));

                    // $onair_4g =
                }
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                Log::error($e->getMessage());
            }

        }else {
            return response('Unauthorized', 400);
        }
    }
}
