<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Log;

class DocumentController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }

    public function bts_summary_1(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        if ($request->input('date') == null) {
            $month_start = date('m');
            $year_start = date('Y');
        }else {
            $month_start = date('m', strtotime($request->input('date')));
            $year_start = date('Y', strtotime($request->input('date')));
        }

        for ($i=1; $i < 7 ; $i++) {
            $month = $month_start - $i;
            $year = $year_start;
            if ($month <= 0) {
                $month = $month + 12;
                $year = $year - 1;
            }

            $data[$i - 1] = DB::connection('mysql2')
            ->table('resume_nodin_bts_monthly')
            ->whereMonth('date', $month)
            ->whereYear('date', $year)
            ->orderBy('date', 'desc')
            ->first();

            if ($data[$i - 1] == null) {
                $data[$i - 1] = DB::connection('mysql2')
                ->table('resume_nodin_bts_monthly')
                ->whereDate('date', 0)
                ->first();
            }

        }

        return response()->json($data);
    }

    public function bts_summary_2(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = DB::connection('mysql2')
        ->table('resume_sysinfo')
        ->selectRaw('
            REGIONAL as regional,
            bsc as data1,
            bts as data2,
            rnc as data3,
            nodeb as data4,
            enodeb as data5
        ')
        ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
        ->orderByRaw('substr(REGIONAL, 9, 2)*1 asc')
        ->get()->toArray();
        $data_total = DB::connection('mysql2')
        ->table('resume_sysinfo')
        ->selectRaw('
            "Total" as regional,
            SUM(bsc) as data1,
            SUM(bts) as data2,
            SUM(rnc) as data3,
            SUM(nodeb) as data4,
            SUM(enodeb) as data5
        ')
        ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
        ->orderByRaw('substr(REGIONAL, 9, 2)*1 asc')
        ->first();
        array_push($data, $data_total);

        return response()->json($data);
    }

    public function bts_summary_2_monthly1(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        if ($request->input('date') == null) {
            $month_start = date('m');
            $year_start = date('Y');
        }else {
            $month_start = date('m', strtotime($request->input('date')));
            $year_start = date('Y', strtotime($request->input('date')));
        }

        for ($i=1; $i < 2 ; $i++) {
            $month = $month_start - $i;
            $year = $year_start;
            if ($month <= 0) {
                $month = $month + 12;
                $year = $year - 1;
            }

            $data[$i - 1] = DB::connection('mysql2')
            ->table('resume_sysinfo_monthly')
            ->whereMonth('date', $month)
            ->whereYear('date', $year)
            ->selectRaw('
            REGIONAL as regional,
            bsc as data1,
            bts as data2,
            rnc as data3,
            nodeb as data4,
            enodeb as data5
            ')
            ->orderByRaw('substr(REGIONAL, 9, 2)*1 asc')
            ->get()->toArray();

            if ($data[$i - 1] == null) {
                $data[$i - 1] = DB::connection('mysql2')
                ->table('resume_sysinfo_monthly')
                ->whereDate('date', 0)
                ->get()->toArray();
            }
        }

        return response()->json($data);
    }

    public function bts_summary_2_monthly()
    {
        date_default_timezone_set('Asia/Jakarta');
        $data = DB::connection('mysql2')
        ->table('resume_sysinfo_monthly')
        ->selectRaw('
        REGIONAL as regional,
        bsc as data1,
        bts as data2,
        rnc as data3,
        nodeb as data4,
        enodeb as data5
        ')
        ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
        ->orderByRaw('substr(REGIONAL, 9, 2)*1 asc')
        ->get()->toArray();

        $data_total = DB::connection('mysql2')
        ->table('resume_sysinfo_monthly')
        ->selectRaw('
        "Total" as regional,
        SUM(bsc) as data1,
        SUM(bts) as data2,
        SUM(rnc) as data3,
        SUM(nodeb) as data4,
        SUM(enodeb) as data5
        ')
        ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
        ->orderByRaw('substr(REGIONAL, 9, 2)*1 asc')
        ->first();
        array_push($data, $data_total);

        return response()->json($data);
    }

    public function bts_summary_3()
    {
        date_default_timezone_set('Asia/Jakarta');
        // $json = ' [
        //     {"region":"R3 Jabotabek ", "data1": "BOO751", "data2": "DESAPASIRANGINMG", "data3": "LTE1800", "data4": "Huawei", "data5": " Takeout, achievment Mei 2018"},
        //     {"region":"R3 Jabotabek ", "data1": "JSX386", "data2": "PLZOLEOSTOWERMACML", "data3": "LTE1800-INDOOR ", "data4": "Huawei", "data5": " Takeout, achievment Mei 2018"},
        //     {"region":"R3 Jabotabek ", "data1": "TNG468", "data2": "LAMPSITEITCBSD", "data3": "LTE1800-INDOOR ", "data4": "Huawei", "data5": " Takeout, achievment Mei 2018"}
        //     ]';
        // $json = json_decode($json);
        $data = DB::table('t_sum_bts_nodeb_final_final')
        ->selectRaw('
        REGIONAL as region,
        SITE_ID as data1,
        BTS_NODE_NAME as data2,
        FREQ as data3
        ')
        ->where('STATUS', 9)
        ->get();

        return response()->json($data);
    }

    public function bts_summary_4()
    {
        date_default_timezone_set('Asia/Jakarta');
        $data = DB::table('t_insert_manual')
        ->selectRaw('
        region,
        site_no as data1,
        sow as data2,
        band as data3,
        vendor as data4,
        remark as data5
        ')
        ->get();

        return response()->json($data);
    }

    public function table_list_mom()
    {
        $data = DB::table('t_list_report')
        ->selectRaw('
        id_ticket as title,
        input_date,
        "rizkiadr" as submit_by,
        "Approved" as aprstatus
        ')
        ->where('id_ticket', 'like', '%BTSMOM%')
        ->where('status', 1)
        ->get();

        $i=1;
        foreach ($data as $row) {
            $row->no = $i;
            $i++;
        }

        return response()->json($data);
    }

    public function table_list_nodin()
    {
        $data = DB::table('t_list_report')
        ->selectRaw('
        id_ticket as title,
        input_date,
        "rizkiadr" as submit_by,
        "Approved" as aprstatus
        ')
        ->where('id_ticket', 'like', '%BTSNODIN%')
        ->where('status', 1)
        ->get();

        $i=1;
        foreach ($data as $row) {
            $row->no = $i;
            $i++;
        }

        return response()->json($data);
    }

    public function bts_summary_dismantle(){
        $json = ' [
            {"region":"REG2 ", "vendor": "HUAWEI", "ne_id": "BDL157ML1", "enodeb_name": "BDL157ML1_DMT_WayLimus", "band": "4G", "siteid": "BDL157", "band_type": "FDD LTE1800", "remark": "Habis kontrak, nodin 042/TC.01/RI/52/XI/2018"},
            {"region":"REG2 ", "vendor": "HUAWEI", "ne_id": "BDL157MW1", "enodeb_name": "BDL157MW1_DMT_WayLimus", "band": "3G", "siteid": "BDL157", "band_type": "UMTS 2100", "remark": "Habis kontrak, nodin 042/TC.01/RI/52/XI/2018"}
            ]';
            // $json = json_decode($json);

            // return response()->json($json);
            return response($json);

        }

        public function bts_summary_check(){
            $json = '[
                {"region":"R10 Sumbagteng","vendor":"IED","siteno":"UJT539","oss_site_name":"E_UJT539MM1_KEL_BUKTI_DARMA","band":"4G","band_type":"L900","remark":"DB tidak ada di stylo"},
                {"region":"R10 Sumbagteng","vendor":"IED","siteno":"UJT203","oss_site_name":"N_UJT203MM1_CALTEX_PAGER_AREA","band":"4G","band_type":"L900","remark":"DB tidak ada di stylo"}
            ]
            ';

            return response($json);
        }

        public function bts_summary_needcheck_2g(){
            $json = '[
                {"date":"2018-06-01","regional1":"0","regional2":"0","regional3":"8","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"0","grand_total":"8","band":"2G"},
                {"date":"2018-07-01","regional1":"0","regional2":"1","regional3":"0","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"2","grand_total":"3","band":"2G"},
                {"date":"2018-08-01","regional1":"0","regional2":"0","regional3":"0","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"2","regional9":"0","regional10":"0","regional11":"0","grand_total":"2","band":"2G"},
                {"date":"2018-09-01","regional1":"0","regional2":"0","regional3":"150","regional4":"0","regional5":"0","regional6":"0","regional7":"30","regional8":"0","regional9":"0","regional10":"0","regional11":"0","grand_total":"180","band":"2G"},
                {"date":"2018-10-01","regional1":"6","regional2":"0","regional3":"8","regional4":"0","regional5":"16","regional6":"0","regional7":"0","regional8":"6","regional9":"0","regional10":"0","regional11":"4","grand_total":"32","band":"2G"},
                {"date":"2018-11-01","regional1":"7","regional2":"0","regional3":"8","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"24","regional11":"0","grand_total":"31","band":"2G"},
                {"date":"2018-12-01","regional1":"24","regional2":"14","regional3":"8","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"11","regional9":"0","regional10":"0","regional11":"0","grand_total":"49","band":"2G"}
            ]
            ';

            return response($json);
        }

        public function bts_summary_needcheck_3g(){
            $json = '[
                {"date":"2018-01-01","regional1":"0","regional2":"0","regional3":"1","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"0","grand_total":"1","band":"3G"},
                {"date":"2018-06-01","regional1":"0","regional2":"0","regional3":"6","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"4","grand_total":"10","band":"3G"},
                {"date":"2018-07-01","regional1":"0","regional2":"3","regional3":"0","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"1","grand_total":"4","band":"3G"},
                {"date":"2018-08-01","regional1":"0","regional2":"0","regional3":"0","regional4":"2","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"8","regional10":"0","regional11":"0","grand_total":"10","band":"3G"},
                {"date":"2018-09-01","regional1":"0","regional2":"0","regional3":"86","regional4":"0","regional5":"0","regional6":"0","regional7":"12","regional8":"0","regional9":"16","regional10":"0","regional11":"0","grand_total":"114","band":"3G"},
                {"date":"2018-10-01","regional1":"0","regional2":"0","regional3":"7","regional4":"8","regional5":"9","regional6":"0","regional7":"0","regional8":"10","regional9":"0","regional10":"0","regional11":"1","grand_total":"35","band":"3G"},
                {"date":"2018-11-01","regional1":"48","regional2":"102","regional3":"1","regional4":"0","regional5":"349","regional6":"0","regional7":"31","regional8":"0","regional9":"0","regional10":"19","regional11":"0","grand_total":"550","band":"3G"},
                {"date":"2018-12-01","regional1":"0","regional2":"0","regional3":"0","regional4":"0","regional5":"1","regional6":"0","regional7":"6","regional8":"0","regional9":"13","regional10":"0","regional11":"23","grand_total":"43","band":"3G"}
            ]
            ';

            return response($json);
        }

        public function bts_summary_needcheck_4g(){
            $json = '[
                {"date":"2018-07-01","regional1":"0","regional2":"1","regional3":"0","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"0","grand_total":"1","band":"4G"},
                {"date":"2018-08-01","regional1":"0","regional2":"0","regional3":"0","regional4":"0","regional5":"0","regional6":"1","regional7":"0","regional8":"0","regional9":"3","regional10":"0","regional11":"0","grand_total":"4","band":"4G"},
                {"date":"2018-09-01","regional1":"0","regional2":"0","regional3":"33","regional4":"0","regional5":"0","regional6":"0","regional7":"2","regional8":"0","regional9":"5","regional10":"0","regional11":"0","grand_total":"40","band":"4G"}
            ]
            ';

            return response($json);
        }

        public function bts_summary_needcheck(){
            $json = '[
                {"date":"2018-06-01","regional1":"0","regional2":"0","regional3":"8","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"0","grand_total":"8","band":"2G"},
                {"date":"2018-07-01","regional1":"0","regional2":"1","regional3":"0","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"2","grand_total":"3","band":"2G"},
                {"date":"2018-08-01","regional1":"0","regional2":"0","regional3":"0","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"2","regional9":"0","regional10":"0","regional11":"0","grand_total":"2","band":"2G"},
                {"date":"2018-09-01","regional1":"0","regional2":"0","regional3":"150","regional4":"0","regional5":"0","regional6":"0","regional7":"30","regional8":"0","regional9":"0","regional10":"0","regional11":"0","grand_total":"180","band":"2G"},
                {"date":"2018-10-01","regional1":"6","regional2":"0","regional3":"8","regional4":"0","regional5":"16","regional6":"0","regional7":"0","regional8":"6","regional9":"0","regional10":"0","regional11":"4","grand_total":"32","band":"2G"},
                {"date":"2018-11-01","regional1":"7","regional2":"0","regional3":"8","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"24","regional11":"0","grand_total":"31","band":"2G"},
                {"date":"2018-12-01","regional1":"24","regional2":"14","regional3":"8","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"11","regional9":"0","regional10":"0","regional11":"0","grand_total":"49","band":"2G"},
                {"date":"2018-01-01","regional1":"0","regional2":"0","regional3":"1","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"0","grand_total":"1","band":"3G"},
                {"date":"2018-06-01","regional1":"0","regional2":"0","regional3":"6","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"4","grand_total":"10","band":"3G"},
                {"date":"2018-07-01","regional1":"0","regional2":"3","regional3":"0","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"1","grand_total":"4","band":"3G"},
                {"date":"2018-08-01","regional1":"0","regional2":"0","regional3":"0","regional4":"2","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"8","regional10":"0","regional11":"0","grand_total":"10","band":"3G"},
                {"date":"2018-09-01","regional1":"0","regional2":"0","regional3":"86","regional4":"0","regional5":"0","regional6":"0","regional7":"12","regional8":"0","regional9":"16","regional10":"0","regional11":"0","grand_total":"114","band":"3G"},
                {"date":"2018-10-01","regional1":"0","regional2":"0","regional3":"7","regional4":"8","regional5":"9","regional6":"0","regional7":"0","regional8":"10","regional9":"0","regional10":"0","regional11":"1","grand_total":"35","band":"3G"},
                {"date":"2018-11-01","regional1":"48","regional2":"102","regional3":"1","regional4":"0","regional5":"349","regional6":"0","regional7":"31","regional8":"0","regional9":"0","regional10":"19","regional11":"0","grand_total":"550","band":"3G"},
                {"date":"2018-12-01","regional1":"0","regional2":"0","regional3":"0","regional4":"0","regional5":"1","regional6":"0","regional7":"6","regional8":"0","regional9":"13","regional10":"0","regional11":"23","grand_total":"43","band":"3G"},
                {"date":"2018-07-01","regional1":"0","regional2":"1","regional3":"0","regional4":"0","regional5":"0","regional6":"0","regional7":"0","regional8":"0","regional9":"0","regional10":"0","regional11":"0","grand_total":"1","band":"4G"},
                {"date":"2018-08-01","regional1":"0","regional2":"0","regional3":"0","regional4":"0","regional5":"0","regional6":"1","regional7":"0","regional8":"0","regional9":"3","regional10":"0","regional11":"0","grand_total":"4","band":"4G"},
                {"date":"2018-09-01","regional1":"0","regional2":"0","regional3":"33","regional4":"0","regional5":"0","regional6":"0","regional7":"2","regional8":"0","regional9":"5","regional10":"0","regional11":"0","grand_total":"40","band":"4G"}
            ]
            ';

            return response($json);
        }

        public function resume_ne()
        {
            date_default_timezone_set('Asia/Jakarta');
            $data = DB::connection('mysql2')
            ->table('resume_ne')
            ->selectRaw('
            regional,
            vendor,
            2g_bsc as bsc,
            2g_site as site,
            2g_bts as bts,
            2g_sector as sec2g,
            2g_trx as trx,
            3g_rnc as rnc,
            3g_f1_f2 as f1f2,
            3g_6sector as sec6,
            3g_f3 as f3,
            3g_bts_hotel as btshtl,
            3g_sector as sec3g,
            4g_enodeb as enodeb,
            4g_sector as sec4g,
            input_date
            ')
            ->whereDate('input_date', date('Y-m-d', strtotime('-1 day')))
            ->get()->toArray();
            $data_total = DB::connection('mysql2')
            ->table('resume_ne')
            ->selectRaw('
            "" as regional,
            "" as vendor,
            SUM(2g_bsc) as bsc,
            SUM(2g_site) as site,
            SUM(2g_bts) as bts,
            SUM(2g_sector) as sec2g,
            SUM(2g_trx) as trx,
            SUM(3g_rnc) as rnc,
            SUM(3g_f1_f2) as f1f2,
            SUM(3g_6sector) as sec6,
            SUM(3g_f3) as f3,
            SUM(3g_bts_hotel) as btshtl,
            SUM(3g_sector) as sec3g,
            SUM(4g_enodeb) as enodeb,
            SUM(4g_sector) as sec4g
            ')
            ->whereDate('input_date', date('Y-m-d', strtotime('-1 day')))
            ->first();
            array_push($data, $data_total);

            return response()->json($data);
        }

        public function resume_ne_monthly()
        {
            date_default_timezone_set('Asia/Jakarta');
            $data = DB::connection('mysql2')
            ->table('resume_ne_monthly')
            ->selectRaw('
            regional,
            vendor,
            2g_bsc as bsc,
            2g_site as site,
            2g_bts as bts,
            2g_sector as sec2g,
            2g_trx as trx,
            3g_rnc as rnc,
            3g_f1_f2 as f1f2,
            3g_6sector as sec6,
            3g_f3 as f3,
            3g_bts_hotel as btshtl,
            3g_sector as sec3g,
            4g_enodeb as enodeb,
            4g_sector as sec4g,
            input_date
            ')
            ->whereMonth('input_date', date('m', strtotime('last day of previous month')))
            ->get()->toArray();
            $data_total = DB::connection('mysql2')
            ->table('resume_ne_monthly')
            ->selectRaw('
            "" as regional,
            "" as vendor,
            SUM(2g_bsc) as bsc,
            SUM(2g_site) as site,
            SUM(2g_bts) as bts,
            SUM(2g_sector) as sec2g,
            SUM(2g_trx) as trx,
            SUM(3g_rnc) as rnc,
            SUM(3g_f1_f2) as f1f2,
            SUM(3g_6sector) as sec6,
            SUM(3g_f3) as f3,
            SUM(3g_bts_hotel) as btshtl,
            SUM(3g_sector) as sec3g,
            SUM(4g_enodeb) as enodeb,
            SUM(4g_sector) as sec4g
            ')
            ->whereMonth('input_date', date('m', strtotime('last day of previous month')))
            ->first();
            array_push($data, $data_total);

            return response()->json($data);
        }

        public function resume_site()
        {
            date_default_timezone_set('Asia/Jakarta');
            $data = DB::connection('mysql2')
            ->table('resume_site')
            ->selectRaw('
            regional,
            2g3g as site_2g3g,
            2g3g4g as site_2g3g4g,
            2g4g as site_2g4g,
            2g_only as site_2g,
            3g4g as site_3g4g,
            3g_only as site_3g,
            4g_only as site_4g,
            2g3g + 2g3g4g + 2g4g + 2g_only + 3g4g + 3g_only + 4g_only as total
            ')
            ->whereDate('input_date', date('Y-m-d', strtotime('-1 day')))
            ->get()->toArray();
            $data_total = DB::connection('mysql2')
            ->table('resume_site')
            ->selectRaw('
            "Grand Total" as regional,
            SUM(2g3g) as site_2g3g,
            SUM(2g3g4g) as site_2g3g4g,
            SUM(2g4g) as site_2g4g,
            SUM(2g_only) as site_2g,
            SUM(3g4g) as site_3g4g,
            SUM(3g_only) as site_3g,
            SUM(4g_only) as site_4g,
            SUM(2g3g) + SUM(2g3g4g) + SUM(2g4g) + SUM(2g_only) + SUM(3g4g) + SUM(3g_only) + SUM(4g_only) as total
            ')
            ->whereDate('input_date', date('Y-m-d', strtotime('-1 day')))
            ->first();
            array_push($data, $data_total);

            return response()->json($data);
        }

        public function resume_site_monthly()
        {
            date_default_timezone_set('Asia/Jakarta');
            $data = DB::connection('mysql2')
            ->table('resume_site_monthly')
            ->selectRaw('
            regional,
            2g3g as site_2g3g,
            2g3g4g as site_2g3g4g,
            2g4g as site_2g4g,
            2g_only as site_2g,
            3g4g as site_3g4g,
            3g_only as site_3g,
            4g_only as site_4g,
            2g3g + 2g3g4g + 2g4g + 2g_only + 3g4g + 3g_only + 4g_only as total
            ')
            ->whereMonth('input_date', date('m', strtotime('last day of previous month')))
            ->get()->toArray();
            $data_total = DB::connection('mysql2')
            ->table('resume_site_monthly')
            ->selectRaw('
            "Grand Total" as regional,
            SUM(2g3g) as site_2g3g,
            SUM(2g3g4g) as site_2g3g4g,
            SUM(2g4g) as site_2g4g,
            SUM(2g_only) as site_2g,
            SUM(3g4g) as site_3g4g,
            SUM(3g_only) as site_3g,
            SUM(4g_only) as site_4g,
            SUM(2g3g) + SUM(2g3g4g) + SUM(2g4g) + SUM(2g_only) + SUM(3g4g) + SUM(3g_only) + SUM(4g_only) as total
            ')
            ->whereMonth('input_date', date('m', strtotime('last day of previous month')))
            ->first();
            array_push($data, $data_total);

            return response()->json($data);
        }

        public function chart_onair()
        {
            date_default_timezone_set('Asia/Jakarta');
            $data_total = DB::connection('mysql2')
            ->table('resume_sysinfo')
            ->selectRaw('
            case when regional = "REGIONAL1" THEN "R1"
            WHEN regional = "REGIONAL2" THEN "R2"
            WHEN regional = "REGIONAL3" THEN "R3"
            WHEN regional = "REGIONAL4" THEN "R4"
            WHEN regional = "REGIONAL5" THEN "R5"
            WHEN regional = "REGIONAL6" THEN "R6"
            WHEN regional = "REGIONAL7" THEN "R7"
            WHEN regional = "REGIONAL8" THEN "R8"
            WHEN regional = "REGIONAL9" THEN "R9"
            WHEN regional = "REGIONAL10" THEN "R10"
            WHEN regional = "REGIONAL11" THEN "R11" END AS regional,
            bts + nodeb + enodeb as total
            ')
            ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
            ->get();
            $data_existing = DB::connection('mysql2')
            ->table('resume_sysinfo')
            ->selectRaw('
            case when regional = "REGIONAL1" THEN "R1"
            WHEN regional = "REGIONAL2" THEN "R2"
            WHEN regional = "REGIONAL3" THEN "R3"
            WHEN regional = "REGIONAL4" THEN "R4"
            WHEN regional = "REGIONAL5" THEN "R5"
            WHEN regional = "REGIONAL6" THEN "R6"
            WHEN regional = "REGIONAL7" THEN "R7"
            WHEN regional = "REGIONAL8" THEN "R8"
            WHEN regional = "REGIONAL9" THEN "R9"
            WHEN regional = "REGIONAL10" THEN "R10"
            WHEN regional = "REGIONAL11" THEN "R11" END AS regional,
            bts + nodeb + enodeb as existing
            ')
            ->whereDate('date', date('Y-m-d', strtotime('-2 days')))
            ->get();

            $all_data = [];
            for ($i=0; $i < 11; $i++) {
                $new = $data_total[$i]->total - $data_existing[$i]->existing;
                if ($new < 0) {
                    $new = 0;
                }
                $data = [
                    'regional' => $data_total[$i]->regional,
                    'new' => $new,
                    'existing' => $data_existing[$i]->existing,
                    'total' => $data_total[$i]->total
                ];
                array_push($all_data, $data);
            }

            return response()->json($all_data);
        }

        public function chart_onair_monthly()
        {
            date_default_timezone_set('Asia/Jakarta');
            $data_total = DB::connection('mysql2')
            ->table('resume_sysinfo_monthly')
            ->selectRaw('
            case when regional = "REGIONAL1" THEN "R1"
            WHEN regional = "REGIONAL2" THEN "R2"
            WHEN regional = "REGIONAL3" THEN "R3"
            WHEN regional = "REGIONAL4" THEN "R4"
            WHEN regional = "REGIONAL5" THEN "R5"
            WHEN regional = "REGIONAL6" THEN "R6"
            WHEN regional = "REGIONAL7" THEN "R7"
            WHEN regional = "REGIONAL8" THEN "R8"
            WHEN regional = "REGIONAL9" THEN "R9"
            WHEN regional = "REGIONAL10" THEN "R10"
            WHEN regional = "REGIONAL11" THEN "R11" END AS regional,
            bts + nodeb + enodeb as total
            ')
            ->whereMonth('date', date('m', strtotime('last day of previous month')))
            ->orderByRaw('substr(REGIONAL, 9, 2)*1 asc')
            ->get();
            $data_existing = DB::connection('mysql2')
            ->table('resume_sysinfo_monthly')
            ->selectRaw('
            case when regional = "REGIONAL1" THEN "R1"
            WHEN regional = "REGIONAL2" THEN "R2"
            WHEN regional = "REGIONAL3" THEN "R3"
            WHEN regional = "REGIONAL4" THEN "R4"
            WHEN regional = "REGIONAL5" THEN "R5"
            WHEN regional = "REGIONAL6" THEN "R6"
            WHEN regional = "REGIONAL7" THEN "R7"
            WHEN regional = "REGIONAL8" THEN "R8"
            WHEN regional = "REGIONAL9" THEN "R9"
            WHEN regional = "REGIONAL10" THEN "R10"
            WHEN regional = "REGIONAL11" THEN "R11" END AS regional,
            bts + nodeb + enodeb as existing
            ')
            ->whereMonth('date', date('m', strtotime('first day of this month -2 months')))
            ->orderByRaw('substr(REGIONAL, 9, 2)*1 asc')
            ->get();

            $all_data = [];
            for ($i=0; $i < 11; $i++) {
                $new = $data_total[$i]->total - $data_existing[$i]->existing;
                if ($new < 0) {
                    $new = 0;
                }
                $data = [
                    'regional' => $data_total[$i]->regional,
                    'new' => $new,
                    'existing' => $data_existing[$i]->existing,
                    'total' => $data_total[$i]->total
                ];
                array_push($all_data, $data);
            }

            return response()->json($all_data);
        }

        public function dashboard_2g()
        {
            date_default_timezone_set('Asia/Jakarta');
            $total = DB::connection('mysql2')
            ->table('resume_sysinfo')
            ->selectRaw('
            SUM(bts) as onair_2g
            ')
            ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
            ->first();

            $total->new_2g = 0;

            $offair_2g = DB::connection('mysql2')
            ->table('resume_nodin_bts')
            ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
            ->sum('offair_2g');

            $total->dismantle_2g = $offair_2g;

            return response()->json($total);
        }

        public function dashboard_2g_monthly()
        {
            date_default_timezone_set('Asia/Jakarta');
            $total = DB::connection('mysql2')
            ->table('resume_sysinfo_monthly')
            ->selectRaw('
            SUM(bts) as onair_2g
            ')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->first();

            $total->new_2g = 0;

            $offair_2g = DB::connection('mysql2')
            ->table('resume_nodin_bts_monthly')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->sum('offair_2g');

            $total->dismantle_2g = $offair_2g;

            return response()->json($total);
        }

        public function dashboard_3g()
        {
            date_default_timezone_set('Asia/Jakarta');
            $total = DB::connection('mysql2')
            ->table('resume_sysinfo')
            ->selectRaw('
            SUM(nodeb) as onair_3g
            ')
            ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
            ->first();

            $total->new_3g = 0;

            $offair_3g = DB::connection('mysql2')
            ->table('resume_nodin_bts')
            ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
            ->sum('offair_3g');

            $total->dismantle_3g = $offair_3g;

            return response()->json($total);
        }

        public function dashboard_3g_monthly()
        {
            date_default_timezone_set('Asia/Jakarta');
            $total = DB::connection('mysql2')
            ->table('resume_sysinfo_monthly')
            ->selectRaw('
            SUM(nodeb) as onair_3g
            ')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->first();

            $total->new_3g = 0;

            $offair_3g = DB::connection('mysql2')
            ->table('resume_nodin_bts_monthly')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->sum('offair_3g');

            $total->dismantle_3g = $offair_3g;

            return response()->json($total);
        }

        public function dashboard_4g()
        {
            date_default_timezone_set('Asia/Jakarta');
            $total = DB::connection('mysql2')
            ->table('resume_sysinfo')
            ->selectRaw('
            SUM(enodeb) as onair_4g
            ')
            ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
            ->first();

            $onair_4g = DB::connection('mysql2')
            ->table('resume_nodin_bts')
            ->whereMonth('date', date('m'))
            ->sum('onair_4g');

            $offair_4g = DB::connection('mysql2')
            ->table('resume_nodin_bts')
            ->whereMonth('date', date('m'))
            ->sum('offair_4g');

            $total->new_4g = $onair_4g;
            $total->dismantle_4g = $offair_4g;

            return response()->json($total);
        }

        public function dashboard_4g_monthly()
        {
            date_default_timezone_set('Asia/Jakarta');
            $total = DB::connection('mysql2')
            ->table('resume_sysinfo_monthly')
            ->selectRaw('
            SUM(enodeb) as onair_4g
            ')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->first();

            $onair_4g = DB::connection('mysql2')
            ->table('resume_nodin_bts_monthly')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->sum('onair_4g');
            // dd($onair_4g);

            $offair_4g = DB::connection('mysql2')
            ->table('resume_nodin_bts_monthly')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->sum('offair_4g');

            $total->new_4g = $onair_4g;
            $total->dismantle_4g = $offair_4g;

            return response()->json($total);
        }

        public function dashboard_total()
        {
            date_default_timezone_set('Asia/Jakarta');
            $total = DB::connection('mysql2')
            ->table('resume_sysinfo')
            ->selectRaw('
            SUM(bts) + SUM(nodeb) + SUM(enodeb) as onair_total
            ')
            ->whereDate('date', date('Y-m-d', strtotime('-1 day')))
            ->first();

            $onair_total = DB::connection('mysql2')
            ->table('resume_nodin_bts')
            ->whereMonth('date', date('m'))
            ->sum('onair_4g');

            $offair_total = DB::connection('mysql2')
            ->table('resume_nodin_bts')
            ->selectRaw('
            offair_2g + offair_3g + offair_4g as offair_total
            ')
            ->whereMonth('date', date('m'))
            ->first();

            $total->new_total = $onair_total;
            if ($offair_total != null) {
                $total->dismantle_total = $offair_total->offair_total;
            }else {
                $total->dismantle_total = 0;
            }


            return response()->json($total);
        }

        public function dashboard_total_monthly()
        {
            date_default_timezone_set('Asia/Jakarta');
            $total = DB::connection('mysql2')
            ->table('resume_sysinfo_monthly')
            ->selectRaw('
            SUM(bts) + SUM(nodeb) + SUM(enodeb) as onair_total
            ')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->first();

            $onair_total = DB::connection('mysql2')
            ->table('resume_nodin_bts_monthly')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->sum('onair_4g');

            $offair_total = DB::connection('mysql2')
            ->table('resume_nodin_bts_monthly')
            ->selectRaw('
            offair_2g + offair_3g + offair_4g as offair_total
            ')
            ->whereMonth('date', date('m', strtotime('first day of this month -1 months')))
            ->first();

            $total->new_total = $onair_total;
            if ($offair_total != null) {
                $total->dismantle_total = $offair_total->offair_total;
            }else {
                $total->dismantle_total = 0;
            }


            return response()->json($total);
        }

        // public function insert_nodin(Request $request)
        // {
        //     $id_ticket = $request->input('id_ticket');
        //     $status = $request->input('status');
        //
        //     $insert = DB::table('t_list_report')
        //     ->insert([
        //         'id_ticket' => $id_ticket,
        //         'status' => $status
        //     ]);
        //     return response()->json($insert);
        // }
        //
        // public function approve_nodin(Request $request)
        // {
        //     $id_ticket = $request->input('id_ticket');
        //     $status = $request->input('status');
        //
        //     $insert = DB::table('t_list_report')
        //     ->where('id_ticket', $id_ticket)
        //     ->update([
        //         'status' => $status
        //     ]);
        //
        //     return response()->json('Success', 200);
        // }
    }
