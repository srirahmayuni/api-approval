<?php

namespace App\Http\Controllers;

use App\Nodeb;
use App\Ticketing;
use Illuminate\Http\Request;
use DB;
use Log;
use App\AuthLdap;
use Carbon\Carbon;

class TicketingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function update_ticket(Request $request)
    {
        $approval_status = $request->input('status_approval');
        $data = json_decode($request->input('data'));
        $id_ticket = $request->input('id_ticket');

        foreach ($data as $row) {
            if ($approval_status == "Reject") {
                DB::table('t_sum_bts_nodeb_final')
                ->where('NE_ID', $row->ne_id)
                ->where('ID_TICKET', $id_ticket)
                ->update([
                    'APPROVAL_STATUS' => 4
                ]);
            }elseif ($approval_status == "Approve") {
                $status = DB::table('t_mst_status')
                ->select('STATUS_ID')
                ->where('STATUS_NAME', $row->status)
                ->first();

                DB::table('t_sum_bts_nodeb_final')
                ->where('NE_ID', $row->ne_id)
                ->where('ID_TICKET', $id_ticket)
                ->update([
                    'STATUS' => $status->STATUS_ID,
                    'APPROVAL_STATUS' => 1
                ]);
            }else {
                return response()->json('Status Not Found', 400);
            }
        }

        return response()->json('Success', 200);
    }

    public function update_ticket_unknown(Request $request)
    {
        $approval_status = $request->input('status_approval');
        $data = json_decode($request->input('data'));
        $id_ticket = $request->input('id_ticket');
        // Log::info($data);

        foreach ($data as $row) {
            if ($approval_status == "Reject") {
                DB::table('t_sum_unknown')
                ->where('NE_ID', $row->NE_ID)
                ->where('ID_TICKET', $id_ticket)
                ->update([
                    'APPROVAL_STATUS' => 4
                ]);
            }elseif ($approval_status == "Approve") {
                $status = DB::table('t_mst_status')
                ->where('STATUS_NAME', $row->STATUS)
                ->first();

                DB::table('t_sum_bts_nodeb_final')
                ->where('NE_ID', $row->NEID_OSS)
                ->update([
                    'NE_ID' => $row->NEW_NEID,
                    'STATUS' => $status->STATUS_ID
                ]);

                DB::table('t_sum_unknown')
                ->where('NE_ID', $row->NE_ID)
                ->where('ID_TICKET', $id_ticket)
                ->update([
                    'APPROVAL_STATUS' => 1,
                    'STATUS' => $status->STATUS_ID
                ]);
            }else {
                return response()->json('Status Not Found', 400);
            }
        }
        return response()->json('Success', 200);
    }

    public function update_ticket_mom(Request $request)
    {
        $id_ticket = $request->input('id_ticket');
        $status_approval = $request->input('status_approval');

        if ($status_approval == 'Approve') {
            $update = DB::table('t_list_report')
            ->where('id_ticket', $id_ticket)
            ->update([
                'status' => 1
            ]);

            //Create Ticket Nodin
            $type = 'BTSNODIN/';
            $date = Carbon::now()->format('dmY').'/';
            $check_ticket = DB::table('t_no_ticket')
            ->whereDate('date', date('Y-m-d'))
            ->orderBy('number', 'desc')
            ->first();
            if ($check_ticket) {
                $number = str_pad($check_ticket->number + 1, 3, 0, STR_PAD_LEFT);
                $num = $check_ticket->number + 1;
            }else {
                $number = str_pad(1, 3, 0, STR_PAD_LEFT);
                $num = 1;
            }
            $id_ticket = $type.$date.$number;

            $insert_ticket_nodin = DB::table('t_list_report')
            ->insert([
                'id_ticket' => $id_ticket,
                'status' => 0
            ]);

            $data_vp = [
                'Rizki_Adrianto',
                'Dwi_SG_Utomo',
                'eko_prasetyo',
                'genta_buana'
            ];
            foreach ($data_vp as $vp) {
                $insert_ticket_nodin2 = DB::connection('mysql2')
                ->table('tiket')
                ->insert([
                    'id_ticket' => $id_ticket,
                    'submitted_by' => 'General Manager',
                    'waiting_for' => $vp,
                    'counter' => 1
                ]);
            }

            DB::table('t_no_ticket')
            ->insert([
                'number' => $num,
                'date' => date('Y-m-d')
            ]);

            return response()->json('Success', 200);
        }
    }

    public function update_ticket_nodin(Request $request)
    {
        $id_ticket = $request->input('id_ticket');
        $status = $request->input('status');

        $insert = DB::table('t_list_report')
        ->where('id_ticket', $id_ticket)
        ->update([
            'status' => $status
        ]);

        return response()->json('Success', 200);
    }
}
