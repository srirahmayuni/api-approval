<?php

namespace App\Http\Controllers;

use App\Nodeb;
use App\Ticketing;
use Illuminate\Http\Request;
use DB;
use Log;
use App\AuthLdap;
use Carbon\Carbon;

class NodebController extends Controller
{

    public $user;
    public $cn;
    public $regional;
    public $manager;
    public $user_data;
    
    public function __construct(Request $request)
    {
        $user = DB::table('t_token')
        ->where('token', $request->input('token'))
        ->first();
    
        $ldap = new AuthLdap; 
        $get_info = $ldap->get_info($user->username);

        if ($get_info == "Failed") {
            return response()->json('Unauthorized', 401);
        } 

        $get_kabupaten_fx = DB::table('t_user_ldap')
        ->where('username', $get_info['cn'])
        ->first(); 

        if ($get_kabupaten_fx == null) {
            return response()->json('Unauthorized', 401);
        }

        $kabupaten = $get_kabupaten_fx->kabupaten;
        $get_region = DB::table('mapping_regional_with_kabupaten')
        ->where('kabupaten',$get_info['kabupaten'])
        ->first();

        if($get_region == NULL) {
          $get_region = DB::table('mapping_regional_with_kabupaten')
          ->where('kabupaten',$kabupaten)
          ->first();
        }
        // if ($get_info['kabupaten'] == "MEDAN" || $get_info['kabupaten'] = "TANGERANG SELATAN") {
        //   $kabupaten = $get_kabupaten_fx->kabupaten;
        //
        //   $get_region = DB::table('mapping_regional_with_kabupaten')
        //   ->where('kabupaten', $kabupaten)
        //   ->first();
        // }else {
        //   $get_region = DB::table('mapping_regional_with_kabupaten')
        //   ->where('kabupaten', $get_info['kabupaten'])
        //   ->first();
        // }

        $regional = [
            'R1' => 'REGIONAL1',
            'R2' => 'REGIONAL2',
            'R3' => 'REGIONAL3',
            'R4' => 'REGIONAL4',
            'R5' => 'REGIONAL5',
            'R6' => 'REGIONAL6',
            'R7' => 'REGIONAL7',
            'R8' => 'REGIONAL8',
            'R9' => 'REGIONAL9',
            'R10' => 'REGIONAL10',
            'R11' => 'REGIONAL11'
        ];

        $manager = explode(',', $get_info['manager']);
        $manager = explode('=', $manager[0]);
        $manager = $manager[1];

        $this->regional = $get_region->regional;
        $this->user = $user->username;
        $this->cn = $get_info['cn'];
        $this->manager = $manager;
        $this->user_data = $get_kabupaten_fx;
    }

    public function baseline_on_air_4g_monthly(Request $request)
    {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $query = 'select * from master_baseline_onair_4g_monthly where STATUS_BTS != "DISMANTLE"';

        if ($request->input('search') != null) {
            $query .= 'or CELL_NAME = "'.$request->input('search').'" ';
        }
        if ($request->input('limit') != null) {
            $query .= 'limit '.$request->input('limit').' ';
        }
        if ($request->input('skip') != null) {
            $query .= 'offset '.$request->input('skip').' ';
        }   

        $data = DB::connection('mysql2')
        ->select($query);

        $count = DB::connection('mysql2')
        ->table('master_baseline_onair_4g_monthly')
        ->count(); 
                 
        return [
            'data' => $data,
            'count' => $count
        ];
    }


    public function unknown(Request $request)
    {
        // $time_start = microtime(true);
        ini_set('memory_limit', '1024M');
        $regional = $this->regional;
        $page = $request->input('page');

        // $status_unknown = DB::select('select STATUS_ID from t_mst_status where STATUS_NAME = "UNKNOWN - PROJECT"');

        $query = 'select REGIONAL as regional,
        VENDOR as vendor,
        DATE as date,
        BSC_NAME as bsc_name,
        BTS_NODE_NAME as bts_node_name,
        NE_ID as ne_id,
        SCOPE as scope,
        TYPE_BTS as type_bts,
        ACHV_DATE as achv_date,
        REVENUE as revenue,
        TRAFFIC as traffic,
        CASE WHEN STATUS = 1 THEN "ON AIR - NEW" WHEN STATUS = 2 THEN "ON AIR - EXISTING"
        WHEN STATUS = 3 THEN "ON AIR - SWADAYA" WHEN STATUS = 4 THEN "CONNECTED"
        WHEN STATUS = 5 THEN "REKON - PROJECT" WHEN STATUS = 6 THEN "REKON - REGION"
        WHEN STATUS = 7 THEN "OFF AIR" WHEN STATUS = 8 THEN "DB PLAN"
        WHEN STATUS = 9 THEN "DISMANTLE" WHEN STATUS = 10 THEN "DATABASE SAMPAH"
        WHEN STATUS = 11 THEN "UNKNOWN - PROJECT" WHEN STATUS = 12 THEN "ON AIR - NON REPORTED"
        WHEN STATUS = 13 THEN "ON AIR - RELOKASI" WHEN STATUS = 14 THEN "SWAP / MODERNISASI"
        WHEN STATUS = 15 THEN "UNKNOWN - OSS" ELSE "" END as status,
        DESCRIPTION as description,
        NON_REPORTED as non_reported,
        REMARK as remark,
        NEID_OSS as neid_oss,
        NEW_NEID as new_neid,
        PIC as pic,
        SUBMITTED_BY as submitted_by,
        ID_TICKET as id_ticket,
        CASE WHEN APPROVAL_STATUS = 1 THEN "Approved" WHEN APPROVAL_STATUS = 2 THEN "Waiting For Approval" WHEN APPROVAL_STATUS = 3 THEN "Draft" WHEN APPROVAL_STATUS = 4 THEN "Rejected" ELSE "Unknown" END as aprstatus
        from t_sum_unknown ';
        if ($this->user_data->id_pic != 0) {
            $query .= 'where PIC = "'.$this->user_data->id_pic.'" ';
        }
        if ($page != null) {
            $skip = ($page - 1) * 100;
            $query .= 'offset '.$skip.' ';
        }
        // $query .= 'limit 100';
        $data = DB::select($query);

        // foreach ($data as $row) {
        //     $description = DB::select('select DESC_NAME from t_mst_desc where DESC_ID = "'.$row->description.'"');
        //     // $status = DB::table('t_mst_status')
        //     // ->where('STATUS_ID', $row->status)
        //     // ->first();
        //     // $description = DB::table('t_mst_desc')
        //     // ->where('DESC_ID', $row->description)
        //     // ->first();
        //     $row->description = $description[0]->DESC_NAME;
        // }
        return response()->json($data);
    }

    public function index(Request $request)
    {  
        // $time_start = microtime(true);
        ini_set('memory_limit', '1024M');
        $regional = $this->regional;

        $page = $request->input('page');

        if ($this->user_data->id_pic == 12) {
            $rekon_status = 5;
        }else {
            $rekon_status = 6; 
        } 
        
        $query = 'select REGIONAL as regional,
        VENDOR as vendor,
        INPUT_DATE as date, 
        ENODEB_NAME as bts_node_name,
        NEID as ne_id,
        "" as ne_qty,
        SITEID as site_id,  
        FREQUENCY as freq,
        DATE_ONAIR as achv_date,
        "" as revenue,
        "" as traffic,
        STATUS_BTS as status,
        "" as description, 
        "" as pic,
        "" as submitted_by,
        "" as aprstatus
        from master_baseline_onair_4g_monthly limit 100';
        // from master_baseline_onair_4g_monthly where STATUS = '.$rekon_status.' ';
        
        // if ($this->user_data->id_pic != 0) {
        //     $query .= 'and PIC = "'.$this->user_data->id_pic.'" ';
        // }else {
        //     $query .= 'or STATUS = 5';
        // }

        if ($page != null) {
            $skip = ($page - 1) * 100;
            $query .= 'offset '.$skip.' ';
        }
        // $query .= 'limit 10000';
        $data = DB::connection('mysql2')->select($query);
        return response()->json($data);

        
        // CASE WHEN STATUS_BTS = 1 THEN "ON AIR - NEW" WHEN STATUS = 2 THEN "ON AIR - EXISTING"
        // WHEN STATUS = 3 THEN "ON AIR - SWADAYA" WHEN STATUS = 4 THEN "CONNECTED"
        // WHEN STATUS = 5 THEN "REKON - PROJECT" WHEN STATUS = 6 THEN "REKON - REGION"
        // WHEN STATUS = 7 THEN "OFF AIR" WHEN STATUS = 8 THEN "DB PLAN"
        // WHEN STATUS = 9 THEN "DISMANTLE" WHEN STATUS = 10 THEN "DATABASE SAMPAH"
        // WHEN STATUS = 11 THEN "UNKNOWN - PROJECT" WHEN STATUS = 12 THEN "ON AIR - NON REPORTED"
        // WHEN STATUS = 13 THEN "ON AIR - RELOKASI" WHEN STATUS = 14 THEN "SWAP / MODERNISASI"
        // WHEN STATUS = 15 THEN "UNKNOWN - OSS" ELSE "" END as status,
        // CASE WHEN APPROVAL_STATUS = 1 THEN "Approved" WHEN APPROVAL_STATUS = 2 THEN "Waiting For Approval" WHEN APPROVAL_STATUS = 3 THEN "Draft" WHEN APPROVAL_STATUS = 4 THEN "Rejected" ELSE "Unknown" END as aprstatus
    }

    public function summary()
    {
        $data2g = DB::table('bts_nodeb_all')
        ->where('freq', '2G')
        ->selectRaw('
            SUM(CASE WHEN status = "ON AIR" THEN 1 ELSE 0 END) as on_air,
            SUM(CASE WHEN status = "OFF AIR" THEN 1 ELSE 0 END) as off_air,
            SUM(CASE WHEN status = "ON AIR" THEN 1 ELSE 0 END) + SUM(CASE WHEN status = "OFF AIR" THEN 1 ELSE 0 END) as total
        ')
        ->first();

        $data3g = DB::table('bts_nodeb_all')
        ->where('freq', '3G')
        ->selectRaw('
            SUM(CASE WHEN status = "ON AIR" THEN 1 ELSE 0 END) as on_air,
            SUM(CASE WHEN status = "OFF AIR" THEN 1 ELSE 0 END) as off_air,
            SUM(CASE WHEN status = "ON AIR" THEN 1 ELSE 0 END) + SUM(CASE WHEN status = "OFF AIR" THEN 1 ELSE 0 END) as total
        ')
        ->first();

        $data4g = DB::table('bts_nodeb_all')
        ->where('freq', '4G')
        ->selectRaw('
            SUM(CASE WHEN status = "ON AIR" THEN 1 ELSE 0 END) as on_air,
            SUM(CASE WHEN status = "OFF AIR" THEN 1 ELSE 0 END) as off_air,
            SUM(CASE WHEN status = "ON AIR" THEN 1 ELSE 0 END) + SUM(CASE WHEN status = "OFF AIR" THEN 1 ELSE 0 END) as total
        ')
        ->first();

        $data_send = [
            '2G' => $data2g,
            '3G' => $data3g,
            '4G' => $data4g
        ];

        return response()->json($data_send);
    }

    public function get_status()
    {
        $data = DB::table('t_mst_status')
        ->get();

        return response()->json($data);
    }

    public function insert(Request $request)
    {
        $data = [];
        foreach ($request->all() as $req) {
            if (!is_array($req)) {
                continue;
            }
            $key = strtoupper($req['field']);
            $data[$key] = $req['value'];
        }

        if ($this->user_data->id_pic == 12) {
            $data['STATUS'] = 5;
        }else {
            $data['STATUS'] = 6;
        }
        // $data['DESCRIPTION'] = 6;
        // if (isset($data['REGIONAL'])) {
        //     $get_pic = DB::table('t_mst_pic')
        //     ->where('')
        // }
        if (isset($data['NE_ID']) == FALSE) {
            return response()->json('NE ID is null ', 400);
        }

        $check = DB::table('t_sum_bts_nodeb_final')
        ->where('NE_ID', $data['NE_ID'])
        ->first();

        if ($check) {
            return response()->json('NE ID Already Exist', 400);
        }else {
            $insert = DB::table('t_sum_bts_nodeb_final')
            ->insert($data);

            return response()->json('Success', 200);
        }
    }

    public function update_unknown(Request $request)
    {
        if (!isset($request->all()[0]['ne id'])) {
            return response()->json('NE ID is Null', 400);
        }
        if ($request->all()[0]['ne id'] == null) {
            return response()->json('NE ID is Null', 400);
        }

        $type = 'BTSUNKNOWN/';
        $date = Carbon::now()->format('dmY').'/';
        $check_ticket = DB::table('t_no_ticket')
        ->whereDate('date', date('Y-m-d'))
        ->orderBy('number', 'desc')
        ->first();
        if ($check_ticket) {
            $number = str_pad($check_ticket->number + 1, 3, 0, STR_PAD_LEFT);
            $num = $check_ticket->number + 1;
        }else {
            $number = str_pad(1, 3, 0, STR_PAD_LEFT);
            $num = 1;
        }
        $id_ticket = $type.$date.$number;

        $data = [];
        $data_send = [];
        foreach ($request->all() as $req) {
            if (!is_array($req)) {
                continue;
            }
            $key = strtoupper($req['field']);
            $data[$key] = $req['value'];
        }

        if (isset($data['STATUS'])) {
            $check_status = DB::table('t_mst_status')
            ->where('STATUS_NAME', $data['STATUS'])
            ->first();

            if ($check_status) {
                $data['STATUS'] = 11;
            }else {
                return response()->json('Status Not Found', 400);
            }
        }
        $data['ID_TICKET'] = $id_ticket;
        $data['SUBMITTED_BY'] = $this->user;
        $data['APPROVAL_STATUS'] = 2;

        $update = DB::table('t_sum_unknown')
        ->where('NE_ID', $request->all()[0]['ne id'])
        ->update($data);

        if ($update) {
            $data_want_to_send = [
                'NE_ID' => $request->all()[0]['ne id'],
                'STATUS' => $check_status->STATUS_NAME,
                'REMARK' => $data['REMARK'],
                'NEID_OSS' => $data['NEID_OSS'],
                'NEW_NEID' => $data['NEW_NEID']
            ];
            array_push($data_send, $data_want_to_send);
            $data_ticket = [
                'id_ticket' => $id_ticket,
                'submitted_by' => $this->cn,
                'manager' => $this->manager,
                'data' => $data_send
            ];

            $send_data = Ticketing::post_ticket($data_ticket);

            DB::table('t_no_ticket')
            ->insert([
                'number' => $num,
                'date' => date('Y-m-d')
            ]);
        }

        return response()->json('Success', 200);
    }

    public function update(Request $request)
    {
        // Log::info($request->all());
        if (empty($request->all())) {
            return response()->json('Error : Required Parameter Must Be Sent', 400);
        }
        //GENERATE ID TICKET
        $is_ticketing = 0;

        $type = 'BTS/';
        $date = Carbon::now()->format('dmY').'/';
        $check_ticket = DB::table('t_no_ticket')
        ->whereDate('date', date('Y-m-d'))
        ->orderBy('number', 'desc')
        ->first();
        if ($check_ticket) {
            $number = str_pad($check_ticket->number + 1, 3, 0, STR_PAD_LEFT);
            $num = $check_ticket->number + 1;
        }else {
            $number = str_pad(1, 3, 0, STR_PAD_LEFT);
            $num = 1;
        }
        $id_ticket = $type.$date.$number;

        $messages = [];
        $messages_ticket = [];
        foreach ($request->all() as $req) {
            if (!is_array($req)) {
                continue;
            }

            $check = DB::table('t_sum_bts_nodeb_final')
            ->where('NE_ID', $req['id']);
            if ($this->user_data->id_pic != 0) {
                $check = $check->where('PIC', $this->user_data->id_pic);
            }
            $check = $check->first();

            $status = DB::table('t_mst_status')
            ->where('STATUS_NAME', $req['name'])
            ->first();

            if ($check->STATUS == $status->STATUS_ID) {
                $approval_status = 1;//approved
            }else {
                $approval_status = 2;//waiting for approval
            }

            if ($check && $status) {
                if ($approval_status == 2) {
                    $is_ticketing = 1;
                    $nodeb = DB::table('t_sum_bts_nodeb_final')
                    ->where('NE_ID', $req['id'])
                    ->update([
                        // 'STATUS' => $status->STATUS_ID,
                        'APPROVAL_STATUS' => $approval_status,
                        'SUBMITTED_BY' => $this->user,
                        'ID_TICKET' => $id_ticket
                    ]);
                    $message = [
                        'ne_id' => $req['id'],
                        'message' => 'Updated Successfully'
                    ];
                    $message_ticket = [
                        'ne_id' => $req['id'],
                        'status' => $req['name']
                    ];
                    array_push($messages_ticket, $message_ticket);
                    array_push($messages, $message);
                }

            }elseif ($check && $status == null) {
                $message = [
                    'ne_id' => $req['id'],
                    'message' => 'Status Not Found'
                ];
                array_push($messages, $message);
            }else {
                $message = [
                    'ne_id' => $req['id'],
                    'message' => 'Unauthorized'
                ];
                array_push($messages, $message);
            }
        }

        if ($is_ticketing == 1) {
            $data_ticket = [
                'id_ticket' => $id_ticket,
                'submitted_by' => $this->cn,
                'manager' => $this->manager,
                'data' => $messages_ticket
            ];

            $send_data = Ticketing::post_ticket($data_ticket);

            DB::table('t_no_ticket')
            ->insert([
                'number' => $num,
                'date' => date('Y-m-d')
            ]);
        }

        return response()->json($messages, 200);
    }

    public function upload(Request $request)
    {
        if (empty($request->all())) {
            return response()->json('Error : Required Parameter Must Be Sent', 400);
        }

        $array = [];
        $listJson = str_replace("\\", "", $request->all()['listJson']);
        $listJson = str_replace("'", "", $listJson);
        $listJson = json_decode($listJson);
        // return json_decode($listJson);
        // Log::info($request->all()['listJson']);
        $i=0;
        foreach ($listJson as $list) {
            $filename = $request->file('file')->getClientOriginalName();
            $fileextension = $request->file('file')->getClientOriginalExtension();
            $filepath = explode('index.php', $_SERVER['SCRIPT_FILENAME']);
            $destination_path = $filepath[0].'nodin/data/';
            if ($i == 0) {
                $move = $request->file('file')->move($filepath[0].'nodin/data', $list.'.'.$fileextension);
                $first_filename = $list.'.'.$fileextension;
            }else {
                $copy = copy($destination_path.$first_filename, $destination_path.$list.'.'.$fileextension);
            }

            $url = url('/nodin/data/'.$list.'.'.$fileextension);
            $array_push = [
                'list' => $list,
                'url' => $url
            ];
            array_push($array, $array_push);

            // $update = DB::table('t_sum_bts_nodeb_final')
            // ->where('bts_node_name', $list)
            // ->update([
            //     'document_url' => $url
            // ]);
            $i++;
        }

        return response()->json($array);
    }

}
