<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Log;
use Carbon\Carbon;

class IntegrasiAgataController extends Controller{
    const FTP = 'http://10.54.36.38/api/hadoop/query';
    const TOKEN = 'http://10.54.36.38/api/v1/token';
    const AGATA = 'http://10.54.36.50/api-agata-external/post/cell/update_status?api_key=a120e72ccd6bbad70aa76ed6cac81faba5ede0c4';

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function test_connection(){
        $url = self::AGATA;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    'lac'       => 1001,
                    'ci'        => 11,
                    'status'    => 'PROPOSE OFFAIR' 
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }
        
        return json_decode($res->getBody(),true);
    }

    public function test_traffic(){
        ini_set('memory_limit','-1');
        ini_set('max_execution_time',86400);
        $data_status = [];

        $datas = DB::connection('mysql2')
        ->select('SELECT b.cell,b.lac,b.ci,b.payload_total_mbyte FROM auto_stylo_master_cell_3g s 
        JOIN auto_traffic_erricson_3g b ON s.CELL_NAME = b.cell
        WHERE b.payload_total_mbyte IS NOT NULL AND b.payload_total_mbyte > 0');

        $url = self::AGATA;
        $client = new \GuzzleHttp\Client;
        try {
            foreach ($datas as $row) {
                $res = $client->request('POST',$url,[
                    'json' => [
                        'lac'       => $row->lac,
                        'ci'        => $row->ci,
                        'status'    => 'ONAIR' 
                    ]
                ]);

                array_push($data_status,json_decode($res->getBody()));
            }
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        return response($data_status,200);
    }

    public function get_token_bigdata(){
        $url = self::TOKEN.'?username=neisa&password=Neisa&action=login';
        $client = new \GuzzleHttp\Client();

        try {
            $res = $client->request('GET',$url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        return json_decode($res->getBody(), TRUE);
    }

    //NOKIA
    //GET TRAFFIC FROM BIG DATA NOKIA
    public function get_ftp_file_nokia(){
        $token = "";
        $get_token = self::get_token_bigdata();
        $token = $get_token['token'];

        //4G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;

        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "4G_RAN_NOK_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_nokia_4g",
                    "columns"     => [
                      "total_traffic_volume_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GguzzleException $e) {
            return $e->getMessage();
        }

        //3G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;

        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "3G_RAN_NOK_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_nokia_3g",
                    "columns"     => [
                      "payload_total_mbyte"
                    ],
                    "agg_func"    => []
                ] 
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        //2G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;

        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "2G_RAN_NOK_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_nokia_2g",
                    "columns"     => [
                      "total_payload_mbyte"
                    ],
                    "agg_func"    => []
                ] 
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }
    }

    //HUAWEI
    //GET TRAFFIC FROM BIG DATA HUAWEI
    public function get_ftp_file_huawei(){
        $token = "";
        $get_token = self::get_token_bigdata();
        $token = $get_token['token'];

        //4G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "4G_RAN_HUA_2",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_huawei_4g",
                    "columns"     => [
                      "total_traffic_volume_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        //3G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "3G_RAN_HUA_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_huawei_3g",
                    "columns"     => [
                      "payload_total_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        //2G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "2G_RAN_HUA_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_huawei_2g",
                    "columns"     => [
                      "total_payload_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        } 
        
        return json_decode($res->getBody(),true);
    }

    //ZTE
    //GET TRAFFIC FROM BIG DATA ZTE
    public function get_ftp_file_zte(){
        $token = "";
        $get_token = self::get_token_bigdata();
        $token = $get_token['token'];

        //4G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "4G_RAN_ZTE_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_zte_4g",
                    "columns"     => [
                      "total_traffic_volume_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        //3G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "3G_RAN_ZTE_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_zte_3g",
                    "columns"     => [
                      "payload_total_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        //2G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "2G_RAN_ZTE_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_zte_2g",
                    "columns"     => [
                      "total_payload_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        return json_decode($res->getBody(),true);
    }

    //ERRICSON
    //GET TRAFFIC FROM BIGDATA ERRICSON
    public function get_ftp_file_erricson(){
        $token = "";
        $get_token = self::get_token_bigdata();
        $token = $get_token['token'];

        //4G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "4G_RAN_ERI_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_erricson_4g",
                    "columns"     => [
                      "total_traffic_volume_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        //3G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "3G_RAN_ERI_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_erricson_3g",
                    "columns"     => [
                      "payload_total_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }
        
        //2G
        $url = self::FTP.'?token='.$token;
        $client = new \GuzzleHttp\Client;
        try {
            $res = $client->request('POST',$url,[
                'json' => [
                    "tablename"   => "2G_RAN_ERI_1",
                    "agg_mode"    => "auto",
                    "granularity" => "total",
                    "start_time"  => date('Y-m-d 00:00:00',strtotime('-1 month')),
                    "finish_time" => date('Y-m-d 00:00:00',strtotime('now')),
                    "ftp_ip"      => "10.54.36.49",
                    "ftp_user"    => "rizkiadr",
                    "ftp_password"=> "Subaru$!",
                    "ftp_path"    => "/home/rizkiadr/ftp_btsonair/auto_traffic_erricson_2g",
                    "columns"     => [
                      "total_payload_mbyte"
                    ],
                    "agg_func"    => []
                ]
            ]);         
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getMessage();
        }

        return json_decode($res->getBody(),true);
    }
}