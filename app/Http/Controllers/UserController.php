<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    protected function salt()
    {
        $salt = md5(rand());

        $check_salt = DB::table('t_user')
        ->where('salt', $salt)
        ->first();

        if ($check_salt == null) {
            return $salt;
        }else {
            self::salt();
        }
    }

    protected function token($email)
    {
        $get = DB::table('t_user')
        ->where('email', $email)
        ->first();

        $token = md5(rand().$get->email.$get->password);
        $check = DB::table('t_token')
        ->where('token', $token)
        ->first();

        if ($check == null) {
            return $token;
        }else {
            self::token();
        }
    }

    public function insert_user(Request $request)
    {
        $username = $request->input('username');
        $email = $request->input('email');
        $salt = self::salt();
        $password = $request->input('password');
        $region = $request->input('region');
        $status = $request->input('status');

        if ($status == null) { 
            $status = 1; //default Status
        }

        if ($username == null || $email == null || $password == null || $region == null || $status == null) {
            return response()->json('Required Parameter Is Null', 400);
        }

        $check_email = DB::table('t_user')
        ->where('email', $email)
        ->first();
        if ($check_email) {
            return response()->json('Email Already Exist', 400);
        }

        $insert = DB::table('t_user')
        ->insert([
            'username' => $username,
            'email' => $email,
            'salt' => $salt,
            'password' => md5($salt.$password),
            'region' => $region,
            'status' => $status
        ]);

        if ($insert) {
            return response()->json('Data Has Been Inserted Successfully', 200);
        }
    }

    public function update_user()
    {
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        $region = $request->input('region');
        $status = $request->input('status');

        $get_user = DB::table('t_user')
        ->where('email', $email)
        ->first();

        if ($password == null) {
            $pwd = $get_user->password;
        }else {
            $pwd = md5($get_user->salt.$password);
        }

        $update = DB::table('t_user')
        ->where('email', $email)
        ->update([
            'username' => $username,
            'email' => $email,
            'password' => $pwd,
            'region' => $region,
            'status' => $status
        ]);

        return response()->json('Data Has Been Updated Successfully', 200);
    }

    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        $check_user = DB::table('t_user')
        ->where("email", $email)
        ->first();

        if ($check_user == null) {
            return response()->json('Login Failed', 401);
        }
        if ($check_user->status == 0) {
            return response()->json('Login Failed', 401);
        }

        $check_credential = DB::table('t_user')
        ->where('email', $email)
        ->where('password', md5($check_user->salt.$password))
        ->first();

        if ($check_credential) {
            $token = self::token($email);
            $insert_token = DB::table('t_token')
            ->insert([
                'token' => $token,
                'end_date' => Carbon::now()->addHours(24)->toDateTimeString(),
                'user_id' => $check_credential->id
            ]);
            return response()->json($token, 200);
        }else {
            return response()->json('Login Failed', 401);
        }
    }

}
