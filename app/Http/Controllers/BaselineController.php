<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Log;
use Carbon\Carbon;

class BaselineController extends Controller
{
    /**
    * Create a new controller instance.
    * 
    * @return void
    */
    public function __construct()
    {
        //
    } 
    // master_baseline_onair_4g_monthly_rekon
    //FORM BASELINE
    public function onair_bts_4g(Request $request) 
    {                            
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M'); 
        
        $query = 'select * from master_baseline_onair_4g_monthly_rekon where FLAG IS NULL '; 
        
        if ($request->input('regional') != null) { 
            $query .= 'AND REGIONAL LIKE "'.$request->input('regional').'"';
        }  
        
        if ($request->input('search') != null) { 
            $query .= 'AND CELL_NAME LIKE "'.$request->input('search').'"';
        }
        
        if ($request->input('limit') != null) {
            $query .= 'limit '.$request->input('limit').' ';
        }   
        
        if ($request->input('skip') != null) {
            $query .= 'offset '.$request->input('skip').' ';
        } 
        
        $data = DB::connection('mysql2')
        ->select($query); 
        
        $count = DB::connection('mysql2')
        ->table('master_baseline_onair_4g_monthly_rekon')
        ->count();
        
        return [
            'data' => $data,
            'count' => $count
        ]; 
    }   
    //FORM INSERT BASELINE
    public function onair_bts_3g(Request $request) 
    {                            
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M'); 
        
        $query = 'select * from master_baseline_onair_4g_monthly_rekon where FLAG = "1" OR FLAG = "2" '; 
        
        if ($request->input('regional') != null) { 
            $query .= 'AND REGIONAL LIKE "'.$request->input('regional').'"';
        }  
        
        if ($request->input('search') != null) { 
            $query .= 'AND CELL_NAME LIKE "'.$request->input('search').'"';
        }
        
        if ($request->input('limit') != null) {
            $query .= 'limit '.$request->input('limit').' ';
        }   
        
        if ($request->input('skip') != null) {
            $query .= 'offset '.$request->input('skip').' ';
        } 
        
        $data = DB::connection('mysql2')
        ->select($query); 
        
        $count = DB::connection('mysql2')
        ->table('master_baseline_onair_4g_monthly_rekon')
        ->count();
        
        return [
            'data' => $data,
            'count' => $count
        ]; 
    }   
    //FORM APPROVAL
    public function onair_bts_2g(Request $request) 
    {                            
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M'); 
        
        $query = 'select * from master_baseline_onair_4g_monthly_rekon where STATUS_RAO = "" AND STATUS_ASSET = "" OR FLAG = "2" '; 
        
        if ($request->input('regional') != null) { 
            $query .= 'AND REGIONAL LIKE "'.$request->input('regional').'"';
        }  
        
        if ($request->input('search') != null) { 
            $query .= 'AND CELL_NAME LIKE "'.$request->input('search').'"';
        }
        
        if ($request->input('limit') != null) {
            $query .= 'limit '.$request->input('limit').' ';
        }   
        
        if ($request->input('skip') != null) {
            $query .= 'offset '.$request->input('skip').' ';
        } 
        
        $data = DB::connection('mysql2')
        ->select($query); 
        
        $count = DB::connection('mysql2')
        ->table('master_baseline_onair_4g_monthly_rekon')
        ->count();
        
        return [
            'data' => $data,
            'count' => $count
        ]; 
    }

    //INSERT LAC/CI FORM BASELINE
    public function onair_bts_3g_update(Request $request) 
    {                             
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M'); 
        
        $datas = [];
        $data = $request->input('data');
        
        foreach($data as $dt){
            $datas = [
                'REGIONAL' => $dt['REGIONAL'],
                'AREA' => $dt['AREA'], 
                'VENDOR' => $dt['VENDOR'],
                'ENODEB_NAME' => $dt['ENODEB_NAME'],
                'NEID' => $dt['NEID'],
                'SITEID' => $dt['SITEID'],
                'TAC' => $dt['TAC'],
                'ENODEBID' => $dt['ENODEBID'],
                'CI' => $dt['CI'],   
                'EARNFCN' => $dt['EARNFCN'],
                'PID' => $dt['PID'],
                'FREQUENCY' => $dt['FREQUENCY'],
                'BANDTYPE' => $dt['BANDTYPE'],
                'BANDWITH' => $dt['BANDWITH'],
                'JMLH_ENODEB' => $dt['JMLH_ENODEB'],
                'JMLH_CELL' => $dt['JMLH_CELL'],    
                'METRO_E' => $dt['METRO_E'],
                'OWNER_LINK' => $dt['OWNER_LINK'],
                'TIPE_LINK' => $dt['TIPE_LINK'],
                'FAR_END_LINK' => $dt['FAR_END_LINK'],
                'TOTAL_BANDWITH' => $dt['TOTAL_BANDWITH'],
                'TANGGAL_ONAIR_LEASE_LINE' => $dt['TANGGAL_ONAIR_LEASE_LINE'],
                'SITE_SIMPUL' => $dt['SITE_SIMPUL'],
                'JUMLAH_SITE_UNDER_SIMPUL' => $dt['JUMLAH_SITE_UNDER_SIMPUL'],
                'STATUS_LOKASI' => $dt['STATUS_LOKASI'],
                'CLUSTER_SALES' => $dt['CLUSTER_SALES'],
                'TYPE_BTS' => $dt['TYPE_BTS'],
                'STATUS' => $dt['STATUS'], 
                'NEW_EXISTING' => $dt['NEW_EXISTING'],
                'ONAIR' => $dt['ONAIR'],
                'DATE_ONAIR' => $dt['DATE_ONAIR'],
                'KPI_PASS' => $dt['KPI_PASS'],
                'DATE_KPI_PASS' => $dt['DATE_KPI_PASS'],
                'REMARK' => $dt['REMARK'],
                'DEPARTEMENT' => $dt['DEPARTEMENT'],
                'TECHNICAL_AREA' => $dt['TECHNICAL_AREA'],
                'LONGITUDE' => $dt['LONGITUDE'], 
                'LATITUDE' => $dt['LATITUDE'],
                'ALAMAT' => $dt['ALAMAT'],
                'KELURAHAN' => $dt['KELURAHAN'],
                'KECAMATAN' => $dt['KECAMATAN'],
                'KABUPATEN' => $dt['KABUPATEN'],
                'PROVINSI' => $dt['PROVINSI'],
                'TOWER_PROVIDER' => $dt['TOWER_PROVIDER'],
                'NAMA_TOWER_PROVIDER' => $dt['NAMA_TOWER_PROVIDER'],
                'STATUS_PLN' => $dt['STATUS_PLN'], 
                'VENDOR_FMC' => $dt['VENDOR_FMC'],
                'STATUS_ASSET' => $dt['STATUS_ASSET'],
                'STATUS_RAO' => $dt['STATUS_RAO'], 
                'STATUS_PROJECT' => $dt['STATUS_PROJECT'],
                'FLAG' => 1,
                'id' => $dt['id'],
            ];
            $data = DB::connection('mysql2')
            ->table('master_baseline_onair_4g_monthly_rekon')
            ->where('id', $dt['id'])
            ->update($datas);    
        } 
        return response()->json($datas); 
    } 

    //INSERT ROW - FORM INSERT BASELINE
    public function sysinfo4g(Request $request) 
    {                              
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');    
        // var_dump($request->input());
        // die();
        $data = DB::connection('mysql2') 
        ->table('master_baseline_onair_4g_monthly_rekon')  
        ->insert([  
            'CELL_NAME' => $request->input('CELL_NAME'),
            'REGIONAL' => $request->input('REGIONAL'), 
            'AREA' => $request->input('AREA'),
            'VENDOR' => $request->input('VENDOR'), 
            'ENODEB_NAME' => $request->input('ENODEB_NAME'),
            'NEID' => $request->input('NEID'),
            'SITEID' => $request->input('SITEID'),
            'TAC' => $request->input('TAC'), 
            'ENODEBID' => $request->input('ENODEBID'),
            'CI' => $request->input('CI'),   
            'EARNFCN' => $request->input('EARNFCN'),
            'PID' => $request->input('PID'), 
            'FREQUENCY' => $request->input('FREQUENCY'),
            'BANDTYPE' => $request->input('BANDTYPE'),
            'BANDWITH' => $request->input('BANDWITH'),
            'JMLH_ENODEB' => $request->input('JMLH_ENODEB'),
            'JMLH_CELL' => $request->input('JMLH_CELL'),    
            'METRO_E' => $request->input('METRO_E'),
            'OWNER_LINK' => $request->input('OWNER_LINK'),
            'TIPE_LINK' => $request->input('TIPE_LINK'),
            'FAR_END_LINK' => $request->input('FAR_END_LINK'),
            'TOTAL_BANDWITH' => $request->input('TOTAL_BANDWITH'),
            'TANGGAL_ONAIR_LEASE_LINE' => $request->input('TANGGAL_ONAIR_LEASE_LINE'),
            'SITE_SIMPUL' => $request->input('SITE_SIMPUL'),
            'JUMLAH_SITE_UNDER_SIMPUL' => $request->input('JUMLAH_SITE_UNDER_SIMPUL'),
            'STATUS_LOKASI' => $request->input('STATUS_LOKASI'),
            'CLUSTER_SALES' => $request->input('CLUSTER_SALES'),
            'TYPE_BTS' => $request->input('TYPE_BTS'),
            'STATUS' => $request->input('STATUS'), 
            'NEW_EXISTING' => $request->input('NEW_EXISTING'),
            'ONAIR' => $request->input('ONAIR'),
            'DATE_ONAIR' => $request->input('DATE_ONAIR'),
            'KPI_PASS' => $request->input('KPI_PASS'),
            'DATE_KPI_PASS' => $request->input('DATE_KPI_PASS'),
            'REMARK' => $request->input('REMARK'),
            'DEPARTEMENT' => $request->input('DEPARTEMENT'),
            'TECHNICAL_AREA' => $request->input('TECHNICAL_AREA'),
            'LONGITUDE' => $request->input('LONGITUDE'), 
            'LATITUDE' => $request->input('LATITUDE'),
            'ALAMAT' => $request->input('ALAMAT'),
            'KELURAHAN' => $request->input('KELURAHAN'),
            'KECAMATAN' => $request->input('KECAMATAN'),
            'KABUPATEN' => $request->input('KABUPATEN'),
            'PROVINSI' => $request->input('PROVINSI'),
            'TOWER_PROVIDER' => $request->input('TOWER_PROVIDER'),
            'NAMA_TOWER_PROVIDER' => $request->input('NAMA_TOWER_PROVIDER'),
            'STATUS_PLN' => $request->input('STATUS_PLN'), 
            'VENDOR_FMC' => $request->input('VENDOR_FMC'),
            'INPUT_DATE' => $request->input('INPUT_DATE'), 
            'STATUS_BTS' => 'UNKNOWN STYLO',
            'FLAG' => 1
        ]); 
        return response()->json($data);
    }

    //UPDATE DATA FORM INSERT BASELINE
    public function onair_bts_2g_update(Request $request) 
    {                             
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M'); 
        
        $datas = [];
        $data = $request->input('data');
        
        foreach($data as $dt){
            $datas = [
                'REGIONAL' => $dt['REGIONAL'],
                'AREA' => $dt['AREA'], 
                'VENDOR' => $dt['VENDOR'],
                'ENODEB_NAME' => $dt['ENODEB_NAME'],
                'CELL_NAME' => $dt['CELL_NAME'],
                'NEID' => $dt['NEID'],
                'SITEID' => $dt['SITEID'],
                'TAC' => $dt['TAC'],
                'ENODEBID' => $dt['ENODEBID'],
                'CI' => $dt['CI'],   
                'EARNFCN' => $dt['EARNFCN'],
                'PID' => $dt['PID'],
                'FREQUENCY' => $dt['FREQUENCY'],
                'BANDTYPE' => $dt['BANDTYPE'],
                'BANDWITH' => $dt['BANDWITH'],
                'JMLH_ENODEB' => $dt['JMLH_ENODEB'],
                'JMLH_CELL' => $dt['JMLH_CELL'],    
                'METRO_E' => $dt['METRO_E'],
                'OWNER_LINK' => $dt['OWNER_LINK'],
                'TIPE_LINK' => $dt['TIPE_LINK'],
                'FAR_END_LINK' => $dt['FAR_END_LINK'],
                'TOTAL_BANDWITH' => $dt['TOTAL_BANDWITH'],
                'TANGGAL_ONAIR_LEASE_LINE' => $dt['TANGGAL_ONAIR_LEASE_LINE'],
                'SITE_SIMPUL' => $dt['SITE_SIMPUL'],
                'JUMLAH_SITE_UNDER_SIMPUL' => $dt['JUMLAH_SITE_UNDER_SIMPUL'],
                'STATUS_LOKASI' => $dt['STATUS_LOKASI'],
                'CLUSTER_SALES' => $dt['CLUSTER_SALES'],
                'TYPE_BTS' => $dt['TYPE_BTS'],
                'STATUS' => $dt['STATUS'], 
                'NEW_EXISTING' => $dt['NEW_EXISTING'],
                'ONAIR' => $dt['ONAIR'],
                'DATE_ONAIR' => $dt['DATE_ONAIR'],
                'KPI_PASS' => $dt['KPI_PASS'],
                'DATE_KPI_PASS' => $dt['DATE_KPI_PASS'],
                'REMARK' => $dt['REMARK'],
                'DEPARTEMENT' => $dt['DEPARTEMENT'],
                'TECHNICAL_AREA' => $dt['TECHNICAL_AREA'],
                'LONGITUDE' => $dt['LONGITUDE'], 
                'LATITUDE' => $dt['LATITUDE'],
                'ALAMAT' => $dt['ALAMAT'],
                'KELURAHAN' => $dt['KELURAHAN'],
                'KECAMATAN' => $dt['KECAMATAN'],
                'KABUPATEN' => $dt['KABUPATEN'],
                'PROVINSI' => $dt['PROVINSI'],
                'TOWER_PROVIDER' => $dt['TOWER_PROVIDER'],
                'NAMA_TOWER_PROVIDER' => $dt['NAMA_TOWER_PROVIDER'],
                'STATUS_PLN' => $dt['STATUS_PLN'], 
                'VENDOR_FMC' => $dt['VENDOR_FMC'],
                'STATUS_ASSET' => $dt['STATUS_ASSET'],
                'STATUS_RAO' => $dt['STATUS_RAO'], 
                'STATUS_PROJECT' => 'Approval',
                'STATUS_BTS' => 'Waiting Approval',
                'FLAG' => 2,
                'id' => $dt['id'],
            ];
            $data = DB::connection('mysql2')
            ->table('master_baseline_onair_4g_monthly_rekon')
            ->where('id', $dt['id'])
            ->update($datas);    
        } 
        return response()->json($datas); 
    } 

    //UPDATE STATUS - FROM APPROVAL STATUS
    public function onair_bts_4g_update(Request $request) 
    {                             
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M'); 
        
        $datas = [];
        $data = $request->input('data');

        // dd($request->input());
        
        foreach($data as $dt){
            // dd($data[0]['CELL_NAME']);
            if ($data[0]['CELL_NAME'] == null) {
                $datas = DB::connection('mysql2')
                ->table('master_baseline_onair_4g_monthly_rekon')
                ->where('NEID', $dt['NEID'])
                ->update([
                    'REGIONAL' => $dt['REGIONAL'],
                    'AREA' => $dt['AREA'], 
                    'VENDOR' => $dt['VENDOR'],
                    'ENODEB_NAME' => $dt['ENODEB_NAME'],
                    'NEID' => $dt['NEID'],
                    'SITEID' => $dt['SITEID'],
                    'TAC' => $dt['TAC'],
                    'ENODEBID' => $dt['ENODEBID'],
                    'CI' => $dt['CI'],   
                    'EARNFCN' => $dt['EARNFCN'],
                    'PID' => $dt['PID'],
                    'FREQUENCY' => $dt['FREQUENCY'],
                    'BANDTYPE' => $dt['BANDTYPE'],
                    'BANDWITH' => $dt['BANDWITH'],
                    'JMLH_ENODEB' => $dt['JMLH_ENODEB'],
                    'JMLH_CELL' => $dt['JMLH_CELL'],    
                    'METRO_E' => $dt['METRO_E'],
                    'OWNER_LINK' => $dt['OWNER_LINK'],
                    'TIPE_LINK' => $dt['TIPE_LINK'],
                    'FAR_END_LINK' => $dt['FAR_END_LINK'],
                    'TOTAL_BANDWITH' => $dt['TOTAL_BANDWITH'],
                    'TANGGAL_ONAIR_LEASE_LINE' => $dt['TANGGAL_ONAIR_LEASE_LINE'],
                    'SITE_SIMPUL' => $dt['SITE_SIMPUL'],
                    'JUMLAH_SITE_UNDER_SIMPUL' => $dt['JUMLAH_SITE_UNDER_SIMPUL'],
                    'STATUS_LOKASI' => $dt['STATUS_LOKASI'],
                    'CLUSTER_SALES' => $dt['CLUSTER_SALES'],
                    'TYPE_BTS' => $dt['TYPE_BTS'],
                    'STATUS' => $dt['STATUS'], 
                    'NEW_EXISTING' => $dt['NEW_EXISTING'],
                    'ONAIR' => $dt['ONAIR'],
                    'DATE_ONAIR' => $dt['DATE_ONAIR'],
                    'KPI_PASS' => $dt['KPI_PASS'],
                    'DATE_KPI_PASS' => $dt['DATE_KPI_PASS'],
                    'REMARK' => $dt['REMARK'],
                    'DEPARTEMENT' => $dt['DEPARTEMENT'],
                    'TECHNICAL_AREA' => $dt['TECHNICAL_AREA'],
                    'LONGITUDE' => $dt['LONGITUDE'], 
                    'LATITUDE' => $dt['LATITUDE'],
                    'ALAMAT' => $dt['ALAMAT'],
                    'KELURAHAN' => $dt['KELURAHAN'],
                    'KECAMATAN' => $dt['KECAMATAN'],
                    'KABUPATEN' => $dt['KABUPATEN'],
                    'PROVINSI' => $dt['PROVINSI'],
                    'TOWER_PROVIDER' => $dt['TOWER_PROVIDER'],
                    'NAMA_TOWER_PROVIDER' => $dt['NAMA_TOWER_PROVIDER'],
                    'STATUS_PLN' => $dt['STATUS_PLN'], 
                    'VENDOR_FMC' => $dt['VENDOR_FMC'],
                    'STATUS_ASSET' => 'Rejected',
                    'STATUS_RAO' => 'Rejected', 
                    'STATUS_PROJECT' => 'Rejected',
                    'FLAG' => NULL
                ]); 
            }elseif($data[0]['CELL_NAME'] != null AND $data[0]['STATUS_ASSET'] == 'Approval' AND $data[0]['STATUS_RAO'] == 'Approval' AND $data[0]['STATUS_PROJECT'] == 'Approval'){
                $datas = DB::connection('mysql2')
                ->table('master_baseline_onair_4g_monthly')
                ->insert([
                    'REGIONAL' => $dt['REGIONAL'],
                    'AREA' => $dt['AREA'], 
                    'VENDOR' => $dt['VENDOR'],
                    'ENODEB_NAME' => $dt['ENODEB_NAME'],
                    'CELL_NAME' => $dt['CELL_NAME'],
                    'NEID' => $dt['NEID'],
                    'SITEID' => $dt['SITEID'],
                    'TAC' => $dt['TAC'],
                    'ENODEBID' => $dt['ENODEBID'],
                    'CI' => $dt['CI'],   
                    'EARNFCN' => $dt['EARNFCN'],
                    'PID' => $dt['PID'],
                    'FREQUENCY' => $dt['FREQUENCY'],
                    'BANDTYPE' => $dt['BANDTYPE'],
                    'BANDWITH' => $dt['BANDWITH'],
                    'JMLH_ENODEB' => $dt['JMLH_ENODEB'],
                    'JMLH_CELL' => $dt['JMLH_CELL'],    
                    'METRO_E' => $dt['METRO_E'],
                    'OWNER_LINK' => $dt['OWNER_LINK'],
                    'TIPE_LINK' => $dt['TIPE_LINK'],
                    'FAR_END_LINK' => $dt['FAR_END_LINK'],
                    'TOTAL_BANDWITH' => $dt['TOTAL_BANDWITH'],
                    'TANGGAL_ONAIR_LEASE_LINE' => $dt['TANGGAL_ONAIR_LEASE_LINE'],
                    'SITE_SIMPUL' => $dt['SITE_SIMPUL'],
                    'JUMLAH_SITE_UNDER_SIMPUL' => $dt['JUMLAH_SITE_UNDER_SIMPUL'],
                    'STATUS_LOKASI' => $dt['STATUS_LOKASI'],
                    'CLUSTER_SALES' => $dt['CLUSTER_SALES'],
                    'TYPE_BTS' => $dt['TYPE_BTS'],
                    'STATUS' => $dt['STATUS'], 
                    'NEW_EXISTING' => $dt['NEW_EXISTING'],
                    'ONAIR' => $dt['ONAIR'],
                    'DATE_ONAIR' => $dt['DATE_ONAIR'],
                    'KPI_PASS' => $dt['KPI_PASS'],
                    'DATE_KPI_PASS' => $dt['DATE_KPI_PASS'],
                    'REMARK' => $dt['REMARK'],
                    'DEPARTEMENT' => $dt['DEPARTEMENT'],
                    'TECHNICAL_AREA' => $dt['TECHNICAL_AREA'],
                    'LONGITUDE' => $dt['LONGITUDE'], 
                    'LATITUDE' => $dt['LATITUDE'],
                    'ALAMAT' => $dt['ALAMAT'],
                    'KELURAHAN' => $dt['KELURAHAN'],
                    'KECAMATAN' => $dt['KECAMATAN'],
                    'KABUPATEN' => $dt['KABUPATEN'],
                    'PROVINSI' => $dt['PROVINSI'],
                    'TOWER_PROVIDER' => $dt['TOWER_PROVIDER'],
                    'NAMA_TOWER_PROVIDER' => $dt['NAMA_TOWER_PROVIDER'],
                    'STATUS_PLN' => $dt['STATUS_PLN'], 
                    'VENDOR_FMC' => $dt['VENDOR_FMC'],
                    'STATUS_ASSET' => $dt['STATUS_ASSET'],
                    'STATUS_RAO' => $dt['STATUS_RAO'], 
                    'STATUS_PROJECT' => $dt['STATUS_PROJECT'],
                    'STATUS_BTS' => 'ON AIR',
                    'FLAG' => 0
                ]);
            }elseif($data[0]['CELL_NAME'] != null){
                $datas = DB::connection('mysql2')
                ->table('master_baseline_onair_4g_monthly_rekon')
                ->where('NEID', $dt['NEID'])
                ->update([
                    'REGIONAL' => $dt['REGIONAL'],
                    'AREA' => $dt['AREA'], 
                    'VENDOR' => $dt['VENDOR'],
                    'ENODEB_NAME' => $dt['ENODEB_NAME'],
                    'CELL_NAME' => $dt['CELL_NAME'],
                    'NEID' => $dt['NEID'],
                    'SITEID' => $dt['SITEID'],
                    'TAC' => $dt['TAC'],
                    'ENODEBID' => $dt['ENODEBID'],
                    'CI' => $dt['CI'],   
                    'EARNFCN' => $dt['EARNFCN'],
                    'PID' => $dt['PID'],
                    'FREQUENCY' => $dt['FREQUENCY'],
                    'BANDTYPE' => $dt['BANDTYPE'],
                    'BANDWITH' => $dt['BANDWITH'],
                    'JMLH_ENODEB' => $dt['JMLH_ENODEB'],
                    'JMLH_CELL' => $dt['JMLH_CELL'],    
                    'METRO_E' => $dt['METRO_E'],
                    'OWNER_LINK' => $dt['OWNER_LINK'],
                    'TIPE_LINK' => $dt['TIPE_LINK'],
                    'FAR_END_LINK' => $dt['FAR_END_LINK'],
                    'TOTAL_BANDWITH' => $dt['TOTAL_BANDWITH'],
                    'TANGGAL_ONAIR_LEASE_LINE' => $dt['TANGGAL_ONAIR_LEASE_LINE'],
                    'SITE_SIMPUL' => $dt['SITE_SIMPUL'],
                    'JUMLAH_SITE_UNDER_SIMPUL' => $dt['JUMLAH_SITE_UNDER_SIMPUL'],
                    'STATUS_LOKASI' => $dt['STATUS_LOKASI'],
                    'CLUSTER_SALES' => $dt['CLUSTER_SALES'],
                    'TYPE_BTS' => $dt['TYPE_BTS'],
                    'STATUS' => $dt['STATUS'], 
                    'NEW_EXISTING' => $dt['NEW_EXISTING'],
                    'ONAIR' => $dt['ONAIR'],
                    'DATE_ONAIR' => $dt['DATE_ONAIR'],
                    'KPI_PASS' => $dt['KPI_PASS'],
                    'DATE_KPI_PASS' => $dt['DATE_KPI_PASS'],
                    'REMARK' => $dt['REMARK'],
                    'DEPARTEMENT' => $dt['DEPARTEMENT'],
                    'TECHNICAL_AREA' => $dt['TECHNICAL_AREA'],
                    'LONGITUDE' => $dt['LONGITUDE'], 
                    'LATITUDE' => $dt['LATITUDE'],
                    'ALAMAT' => $dt['ALAMAT'],
                    'KELURAHAN' => $dt['KELURAHAN'],
                    'KECAMATAN' => $dt['KECAMATAN'],
                    'KABUPATEN' => $dt['KABUPATEN'],
                    'PROVINSI' => $dt['PROVINSI'],
                    'TOWER_PROVIDER' => $dt['TOWER_PROVIDER'],
                    'NAMA_TOWER_PROVIDER' => $dt['NAMA_TOWER_PROVIDER'],
                    'STATUS_PLN' => $dt['STATUS_PLN'], 
                    'VENDOR_FMC' => $dt['VENDOR_FMC'],
                    'STATUS_ASSET' => $dt['STATUS_ASSET'],
                    'STATUS_RAO' => $dt['STATUS_RAO'], 
                    'STATUS_PROJECT' => $dt['STATUS_PROJECT'],
                    'FLAG' => 2
                ]);
            }
            
            if($data[0]['CELL_NAME'] != null AND $data[0]['STATUS_ASSET'] == 'Approval' AND $data[0]['STATUS_RAO'] == 'Approval' AND $data[0]['STATUS_PROJECT'] == 'Approval'){
                $datas = DB::connection('mysql2')
                ->table('master_baseline_onair_4g_monthly_rekon')
                ->where('NEID', $dt['NEID'])
                ->update([
                    'REGIONAL' => $dt['REGIONAL'],
                    'AREA' => $dt['AREA'], 
                    'VENDOR' => $dt['VENDOR'],
                    'ENODEB_NAME' => $dt['ENODEB_NAME'],
                    'CELL_NAME' => $dt['CELL_NAME'],
                    'NEID' => $dt['NEID'],
                    'SITEID' => $dt['SITEID'],
                    'TAC' => $dt['TAC'],
                    'ENODEBID' => $dt['ENODEBID'],
                    'CI' => $dt['CI'],   
                    'EARNFCN' => $dt['EARNFCN'],
                    'PID' => $dt['PID'],
                    'FREQUENCY' => $dt['FREQUENCY'],
                    'BANDTYPE' => $dt['BANDTYPE'],
                    'BANDWITH' => $dt['BANDWITH'],
                    'JMLH_ENODEB' => $dt['JMLH_ENODEB'],
                    'JMLH_CELL' => $dt['JMLH_CELL'],    
                    'METRO_E' => $dt['METRO_E'],
                    'OWNER_LINK' => $dt['OWNER_LINK'],
                    'TIPE_LINK' => $dt['TIPE_LINK'],
                    'FAR_END_LINK' => $dt['FAR_END_LINK'],
                    'TOTAL_BANDWITH' => $dt['TOTAL_BANDWITH'],
                    'TANGGAL_ONAIR_LEASE_LINE' => $dt['TANGGAL_ONAIR_LEASE_LINE'],
                    'SITE_SIMPUL' => $dt['SITE_SIMPUL'],
                    'JUMLAH_SITE_UNDER_SIMPUL' => $dt['JUMLAH_SITE_UNDER_SIMPUL'],
                    'STATUS_LOKASI' => $dt['STATUS_LOKASI'],
                    'CLUSTER_SALES' => $dt['CLUSTER_SALES'],
                    'TYPE_BTS' => $dt['TYPE_BTS'],
                    'STATUS' => $dt['STATUS'], 
                    'NEW_EXISTING' => $dt['NEW_EXISTING'],
                    'ONAIR' => $dt['ONAIR'],
                    'DATE_ONAIR' => $dt['DATE_ONAIR'],
                    'KPI_PASS' => $dt['KPI_PASS'],
                    'DATE_KPI_PASS' => $dt['DATE_KPI_PASS'],
                    'REMARK' => $dt['REMARK'],
                    'DEPARTEMENT' => $dt['DEPARTEMENT'],
                    'TECHNICAL_AREA' => $dt['TECHNICAL_AREA'],
                    'LONGITUDE' => $dt['LONGITUDE'], 
                    'LATITUDE' => $dt['LATITUDE'],
                    'ALAMAT' => $dt['ALAMAT'],
                    'KELURAHAN' => $dt['KELURAHAN'],
                    'KECAMATAN' => $dt['KECAMATAN'],
                    'KABUPATEN' => $dt['KABUPATEN'],
                    'PROVINSI' => $dt['PROVINSI'],
                    'TOWER_PROVIDER' => $dt['TOWER_PROVIDER'],
                    'NAMA_TOWER_PROVIDER' => $dt['NAMA_TOWER_PROVIDER'],
                    'STATUS_PLN' => $dt['STATUS_PLN'], 
                    'VENDOR_FMC' => $dt['VENDOR_FMC'],
                    'STATUS_ASSET' => $dt['STATUS_ASSET'],
                    'STATUS_RAO' => $dt['STATUS_RAO'], 
                    'STATUS_PROJECT' => $dt['STATUS_PROJECT'],
                    'STATUS_BTS' => 'ON AIR',
                    'FLAG' => 0
                ]);
            }   
        } 

        return response()->json($datas);    
    } 


    public function deleteData($id, Request $request){
        $data = DB::connection('mysql2')->table('master_baseline_onair_4g_monthly_rekon')->where('id', '=', $id)->delete();
        return response()->json($data);
    }
    
}
