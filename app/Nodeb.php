<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
/**
 * Description of Nodeb
 *
 * @author In House Dev Program
 */
class Nodeb extends Model
{
    // DB connection
    //protected $connection = 'btsnodeb';

    // Table name
    protected $table = 'bts_nodeb_all';

    // Fillable attributes
    protected $fillable = [
        'id',
        'regional',
        'vendor',
        'date',
        'bts_node_name',
        'ne_id',
        'ne_qty',
        'site_id',
        'freq'
    ];

    protected $hidden = [];

    public static function get_limit($page, $pic)
    {
        $data = DB::table('t_sum_bts_nodeb_final')
        ->selectRaw('
            REGIONAL as regional,
            VENDOR as vendor,
            DATE as date,
            BTS_NODE_NAME as bts_node_name,
            NE_ID as ne_id,
            NE_QTY as ne_qty,
            SITE_ID as site_id,
            FREQ as freq,
            ACHV_DATE as achv_date,
            REVENUE as revenue,
            TRAFFIC as traffic,
            STATUS as status,
            DESCRIPTION as description,
            PIC as pic
        ');
        if ($pic != 0) {
            $data = $data->where('PIC', $pic);
        }
        if ($page != null) {
            $skip = ($page - 1) * 100;
            $data = $data->skip($skip);
        }
        $data = $data->limit(150)
        ->get();

        foreach ($data as $row) {
            $status = DB::table('t_mst_status')
            ->where('STATUS_ID', $row->status)
            ->first();
            $description = DB::table('t_mst_desc')
            ->where('DESC_ID', $row->description)
            ->first();

            $row->status = $status->STATUS_NAME;
            $row->description = $description->DESC_NAME;
        }

        return $data;
    }

    public static function get_limit2($page)
    {
        if ($page == 0 || $page == null) {
            $data = DB::table('t_sum_bts_nodeb_final')
            ->selectRaw('
                REGIONAL as regional,
                VENDOR as vendor,
                DATE as date,
                BTS_NODE_NAME as bts_node_name,
                NE_ID as ne_id,
                NE_QTY as ne_qty,
                SITE_ID as site_id,
                FREQ as freq,
                ACHV_DATE as achv_date,
                REVENUE as revenue,
                TRAFFIC as traffic,
                STATUS as status,
                DESCRIPTION as description
            ')
            ->limit(100)
            ->get();
        }else {
            $skip = ($page - 1) * 100;
            $data = DB::table('t_sum_bts_nodeb_final')
            ->selectRaw('
                REGIONAL as regional,
                VENDOR as vendor,
                DATE as date,
                BTS_NODE_NAME as bts_node_name,
                NE_ID as ne_id,
                NE_QTY as ne_qty,
                SITE_ID as site_id,
                FREQ as freq,
                ACHV_DATE as achv_date,
                REVENUE as revenue,
                TRAFFIC as traffic,
                STATUS as status,
                DESCRIPTION as description
            ')
            ->skip($skip)
            ->limit(100)
            ->get();
        }

        foreach ($data as $row) {
            $status = DB::table('t_mst_status')
            ->where('STATUS_ID', $row->status)
            ->first();
            $description = DB::table('t_mst_desc')
            ->where('DESC_ID', $row->description)
            ->first();

            $row->status = $status->STATUS_NAME;
            $row->description = $description->DESC_NAME;
        }
        return $data;

    }

    public static function get_limit3($page, $region)
    {
        if ($page == 0 || $page == null) {
            $data = DB::table('t_sum_bts_nodeb_final')
            ->selectRaw('
                REGIONAL as regional,
                VENDOR as vendor,
                DATE as date,
                BTS_NODE_NAME as bts_node_name,
                NE_ID as ne_id,
                NE_QTY as ne_qty,
                SITE_ID as site_id,
                FREQ as freq,
                ACHV_DATE as achv_date,
                REVENUE as revenue,
                TRAFFIC as traffic,
                REMEDY as remedy,
                STATUS as status,
                DESCRIPTION as description
            ')
            ->where('regional', $region)
            ->limit(20)
            ->get();
        }else {
            $skip = ($page - 1) * 20;
            $data = DB::table('t_sum_bts_nodeb_final')
            ->selectRaw('
                REGIONAL as regional,
                VENDOR as vendor,
                DATE as date,
                BTS_NODE_NAME as bts_node_name,
                NE_ID as ne_id,
                NE_QTY as ne_qty,
                SITE_ID as site_id,
                FREQ as freq,
                ACHV_DATE as achv_date,
                REVENUE as revenue,
                TRAFFIC as traffic,
                REMEDY as remedy,
                STATUS as status,
                DESCRIPTION as description
            ')
            ->where('regional', $region)
            ->skip($skip)
            ->limit(20)
            ->get();
        }
    }
    // public static function viewNodeb()
    // {
    //     return DB::connection('btsnodeb')
    //         ->table('bts_nodeb_all')
    //
    //         // Selecting field from all joining table
    //         ->select(
    //             'bts_nodeb_all.id',
    //             'bts_nodeb_all.regional',
    //             'bts_nodeb_all.vendor',
    //             'bts_nodeb_all.date',
    //             'bts_nodeb_all.bts_node_name',
    //             'bts_nodeb_all.ne_id',
    //             'bts_nodeb_all.ne_qty'
    //         );
    //
    //
    // }
    //
    // public static function all()
    // {
    //     return DB::connection('eki')
    //         ->table('bts_nodeb_all')
    //
    //         // Selecting field from all joining table
    //         ->select(
    //             'bts_nodeb_all.id',
    //             'bts_nodeb_all.regional',
    //             'bts_nodeb_all.vendor',
    //             'bts_nodeb_all.date',
    //             'bts_nodeb_all.bts_node_name',
    //             'bts_nodeb_all.ne_id',
    //             'bts_nodeb_all.ne_qty'
    //         );
    //
    //
    // }

    // public static function boot() {  //Change if data is "" to NULL
    //     parent::boot();
    //
    //     static::creating(function($model){
    //         foreach ($model->attributes as $key => $value) {
    //             $model->{$key} = empty($value) ? null : $value;
    //         }
    //     });
    // }
}
