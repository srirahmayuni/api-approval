<?php
namespace App;

class AuthLdap {

    public $ldap_host;
    public $ldap_port;
    public $ldap_user;
    public $ldap_pass;
    public $ldap_dn;

    function __construct() {
        // $this->ci =& get_instance();
        self::_init();
    }

    private function _init() {
        $dn = "cn=appsby login,ou=regionalno,ou=Surabaya,dc=telkomsel,dc=co,dc=id";
        $this->ldap_host = '10.2.126.64';
        $this->ldap_port = 389;
        $this->ldap_user = 'TELKOMSEL\rizkiadr';
        $this->ldap_pass = 'Kumala2018#';
        $this->ldap_dn   = $dn;
    } 

    public function get_dn($samaccountname) {
        $ldap['conn'] = ldap_connect($this->ldap_host, $this->ldap_port) or die("Could not connect to ".$this->ldap_host);
        ldap_set_option($ldap['conn'], LDAP_OPT_REFERRALS, 0);
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
        $dn = '';
        if ($ldap['conn']) {
            $bind = @ldap_bind($ldap['conn'], $this->ldap_user, $this->ldap_pass);
            try {
                $search=ldap_search($ldap['conn'], "dc=Telkomsel,dc=co,dc=id", "samaccountname=".$samaccountname);
            } catch (\Exception $e) {
                return $dn;
            }
            if (ldap_count_entries($ldap['conn'],$search)==1) {
                $info = ldap_get_entries($ldap['conn'], $search);
                $dn   = isset($info[0]['distinguishedname'][0]) ? $info[0]['distinguishedname'][0] : '';
            }
        }
        return $dn;
    }

    public function cek_login($user, $password) {
        $dn = self::get_dn($user);
        if ($dn == '') {
            return FALSE;
        } else {
            $ldap['user'] = $user;
            $ldap['pass'] = $password;
            $ldap['dn']   = $dn;
            $ldap['conn'] = ldap_connect($this->ldap_host, $this->ldap_port) or die("Could not connect to {$this->ldap_host}");
            $bind = @ldap_bind($ldap['conn'], $ldap['dn'], $ldap['pass']);
            if ($bind) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function get_account($user) {
        $ldap['conn'] = ldap_connect($this->ldap_host, $this->ldap_port) or die("Could not connect to {$this->ldap_host}");
        ldap_set_option($ldap['conn'], LDAP_OPT_REFERRALS, 0);
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
        $userldap = array();
        if ($ldap['conn']) {
            $bind = @ldap_bind($ldap['conn'], $this->ldap_dn, $this->ldap_pass);
            //$list_ous = array ("Medan", "Batam", "Palembang", "Atrium", "Bandung", "Surabaya", "Semarang", "Denpasar", "UjungPandang", "HeadOffice");
            $list_ous = array ("Batam", "Bandung", "Surabaya", "Semarang", "Denpasar", "HeadOffice");
            //$list_ous = array ("Surabaya");
            $index = 0;
            foreach ($list_ous as $list_ou) {
                $caris = "ou=".$list_ou.",dc=Telkomsel,dc=co,dc=id";
                $search=ldap_search($ldap['conn'], $caris, "samaccountname=*".$user."*");
                $info = ldap_get_entries($ldap['conn'], $search);
                for ($i=0; $i<$info['count']; $i++) {
                    $userldap[$index]['nik']            = isset($info[$i]['extensionattribute1'][0]) ? $info[$i]['extensionattribute1'][0] : '';
                    $userldap[$index]['logonname']      = isset($info[$i]['samaccountname'][0]) ? $info[$i]['samaccountname'][0] : '';
                    $userldap[$index]['cn']             = isset($info[$i]['cn'][0]) ? $info[$i]['cn'][0] : '';
                    //$userldap[$index]['dn']             = isset($info[$i]['distinguishedname'][0]) ? $info[$i]['distinguishedname'][0] : '';
                    //$userldap[$index]['email']          = isset($info[$i]['mail'][0]) ? $info[$i]['mail'][0] : '';
                    //$userldap[$index]['position']       = isset($info[$i]['title'][0]) ? $info[$i]['title'][0] : '';
                    //$userldap[$index]['departement']    = isset($info[$i]['department'][0]) ? $info[$i]['department'][0] : '';
                    //$userldap[$index]['telp']           = isset($info[$i]['telephonenumber'][0]) ? $info[$i]['telephonenumber'][0] : '';
                    //$userldap[$index]['manager']        = isset($info[$i]['manager'][0]) ? $info[$i]['manager'][0] : '';
                    $index++;
                }
            }
        }
        return $userldap;
    }

    public function get_info($samaccountname) {
        $ldap['conn'] = ldap_connect($this->ldap_host, $this->ldap_port) or die("Could not connect to {$this->ldap_host}");
        ldap_set_option($ldap['conn'], LDAP_OPT_REFERRALS, 0);
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
        if ($ldap['conn']) {
            $bind = @ldap_bind($ldap['conn'], $this->ldap_user, $this->ldap_pass);
            $search=ldap_search($ldap['conn'], "dc=Telkomsel,dc=co,dc=id", "samaccountname=".$samaccountname); 
            if (ldap_count_entries($ldap['conn'],$search)==1) {
                $info = ldap_get_entries($ldap['conn'], $search);
                $userldap['info']           = $info[0];
                $userldap['nik']            = isset($info[0]['extensionattribute1'][0]) ? $info[0]['extensionattribute1'][0] : '';
                $userldap['logonname']      = isset($info[0]['samaccountname'][0]) ? $info[0]['samaccountname'][0] : '';
                $userldap['cn']             = isset($info[0]['cn'][0]) ? $info[0]['cn'][0] : '';
                $userldap['dn']             = isset($info[0]['distinguishedname'][0]) ? $info[0]['distinguishedname'][0] : '';
                $userldap['email']          = isset($info[0]['mail'][0]) ? $info[0]['mail'][0] : '';
                $userldap['position']       = isset($info[0]['title'][0]) ? $info[0]['title'][0] : '';
                $userldap['departement']    = isset($info[0]['department'][0]) ? $info[0]['department'][0] : '';
                $userldap['telp']           = isset($info[0]['telephonenumber'][0]) ? $info[0]['telephonenumber'][0] : '';
                $userldap['manager']        = isset($info[0]['manager'][0]) ? $info[0]['manager'][0] : '';
                $userldap['kabupaten']      = isset($info[0]['extensionattribute6'][0]) ? $info[0]['extensionattribute6'][0] : '';
            }
        }
        return $userldap;
    }

    public function get_info_by_cn($samaccountname) {
        $ldap['conn'] = ldap_connect($this->ldap_host, $this->ldap_port) or die("Could not connect to {$this->ldap_host}");
        ldap_set_option($ldap['conn'], LDAP_OPT_REFERRALS, 0);
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
        if ($ldap['conn']) {
            $bind = @ldap_bind($ldap['conn'], $this->ldap_user, $this->ldap_pass);
            $search=ldap_search($ldap['conn'], "dc=Telkomsel,dc=co,dc=id", "cn=".$samaccountname);
            if (ldap_count_entries($ldap['conn'],$search)==1) {
                $info = ldap_get_entries($ldap['conn'], $search);
                $userldap['info']           = $info[0];
                $userldap['nik']            = isset($info[0]['extensionattribute1'][0]) ? $info[0]['extensionattribute1'][0] : '';
                $userldap['logonname']      = isset($info[0]['samaccountname'][0]) ? $info[0]['samaccountname'][0] : '';
                $userldap['cn']             = isset($info[0]['cn'][0]) ? $info[0]['cn'][0] : '';
                $userldap['dn']             = isset($info[0]['distinguishedname'][0]) ? $info[0]['distinguishedname'][0] : '';
                $userldap['email']          = isset($info[0]['mail'][0]) ? $info[0]['mail'][0] : '';
                $userldap['position']       = isset($info[0]['title'][0]) ? $info[0]['title'][0] : '';
                $userldap['departement']    = isset($info[0]['department'][0]) ? $info[0]['department'][0] : '';
                $userldap['telp']           = isset($info[0]['telephonenumber'][0]) ? $info[0]['telephonenumber'][0] : '';
                $userldap['manager']        = isset($info[0]['manager'][0]) ? $info[0]['manager'][0] : '';
                $userldap['kabupaten']      = isset($info[0]['extensionattribute6'][0]) ? $info[0]['extensionattribute6'][0] : '';
            }
        }
        return $userldap;
    }
}
    
    
?>
